﻿//// Copyright (c) Microsoft Corporation. All rights reserved

(function () {
    "use strict";
    window.onload = function () {
        var serviceList = document.getElementById("serviceList");
        serviceList.onchange = onServiceListSelect;
        document.getElementById("runButton").addEventListener("click", sensorSearch, false);
        document.getElementById("sendButton").addEventListener("click", onSendButtonClick, false);
        document.getElementById("sendButtonPhoto").addEventListener("click", onSendPhotoButtonClick, false);
        document.getElementById("disconnectButton").addEventListener("click", onDisconnectButtonClick, false);
        document.getElementById('reset').addEventListener('click', reset, false);
        var storageFolder = Windows.Storage.StorageFolder;
        var photosFolder = Windows.Storage.KnownFolders.picturesLibrary;
        try {
            var folderPromise = photosFolder.getFoldersAsync();
            console.log("Folder is promised");
        } catch (e) {
            console.error(e);
        }
        var POTHOLE_FOLDER;
        try {
            folderPromise.done(function getFoldersSuccess(subfolders) {
                var folderFound = false;
                for (var i = 0; i < subfolders.length; i++) {
                    var subfolder = subfolders[i];
                    console.log(subfolder.name);
                    if (subfolder.name == "pothole") {
                        folderFound = true;
                        POTHOLE_FOLDER = subfolder;
                        console.log("Found folder already");
                    }
                }
                if (!folderFound) {
                    photosFolder.createFolderAsync("pothole").done(function(folder) {
                        POTHOLE_FOLDER = folder;
                        console.log("POTHOLE created");
                    });
                }
            });
        } catch (e) {
            console.error("But you promised!");
            console.error(e);
        }

        onRunButtonClick();
    }
        
    // The Chat Service's custom service Uuid {34B1CF4D-1069-4AD6-89B6-E161D79BE4D8}
    var RFCOMM_CHAT_SERVICE_UUID = "{94f39d29-7d6d-437d-973b-fba39e49d4ee}";
    //var RFCOMM_CHAT_SERVICE_UUID = "{34b1cf4d-1069-4ad6-89b6-e161d79be4d8}";

    // The Id of the Service Name SDP attribute
    var SDP_SERVICE_NAME_ATTRIBUTE_ID = 0x100;

    // The SDP Type of the Service Name SDP attribute.
    // The first byte in the SDP Attribute encodes the SDP Attribute Type as follows :
    //    -  the Attribute Type size in the least significant 3 bits,
    //    -  the SDP Attribute Type value in the most significant 5 bits.
    var SDP_SERVICE_NAME_ATTRIBUTE_TYPE = (4 << 3) | 5;

    var app = WinJS.Application;
    var rfcomm = Windows.Devices.Bluetooth.Rfcomm;
    var sockets = Windows.Networking.Sockets;
    var streams = Windows.Storage.Streams;

    app.addEventListener("oncheckpoint", function (args) {
        // This application is about to be suspended
        disconnect();
        WinJS.log && WinJS.log("Disconnected due to suspension");
    });

    var chatService;
    var chatSocket;
    var chatServices;
    var chatWriter;

    function reset() {
        window.location.reload();
    }
    function sensorSearch() {
        document.getElementById("running_status").innerHTML = "Searching for INFLOW SensorHub";
        console.log("Searching for bluetooth devices");
        Windows.Devices.Enumeration.DeviceInformation.findAllAsync(
            rfcomm.RfcommDeviceService.getDeviceSelector(rfcomm.RfcommServiceId.fromUuid(RFCOMM_CHAT_SERVICE_UUID)),
            null).done(function (services) {
                console.log("Devices: " + services);
                if (services.length > 0) {
                    chatServices = services;

                    while (serviceList.firstChild) {
                        serviceList.removeChild(serviceList.firstChild);
                    }

                    for (var i = 0; i < services.length; i++) {
                        var service = services[i];
                        var serviceName = document.createElement("option");
                        serviceName.dataset.index = i;
                        serviceName.dataset.name = service.name;
                        serviceName.onclick = function (e) {
                            //console.log(e.target.dataset.index);
                            onServiceListSelect(function (ok) {
                                if (!ok) {
                                    console.log(false);
                                    serviceListError.innerHTML = "Cannot connect to " + e.target.dataset.name;
                                } else {
                                    serviceSelector.style.display = "none";
                                    document.getElementById("running_status").innerHTML = "";
                                    document.getElementById("runButton").style.display = "none";
                                }
                            });
                        }
                        serviceName.innerText = service.name + "  -  " + service.id;
                        serviceList.appendChild(serviceName);
                    }

                    serviceList.selectedIndex = -1;
                    serviceSelector.style.display = "";

                    if (services.length == 1) {
                        console.log("Expedite the process");
                        serviceList.selectedIndex = 0;
                        console.log(chatServices[serviceList.selectedIndex]);
                        onServiceListSelect(function (ok) {
                            console.log(ok);
                            if (ok) {
                                serviceSelector.style.display = "none";
                                document.getElementById("running_status").innerHTML = "";
                                document.getElementById("runButton").style.display = "none";
                            } else {
                                serviceSelector.style.display = "";
                                document.getElementById("running_status").innerHTML = "";
                                document.getElementById("runButton").style.display = "";
                            }
                        });
                    }
                } else {
                    console.log("No devices found");
                    WinJS.log && WinJS.log("No chat services were found. Please pair Windows with a device that is " +
                        "advertising the chat service", "sample", "status");
                }
            });
    }
    function onRunButtonClick() {
        sensorSearch();
    }

    function onServiceListSelect(callback) {
        console.log("onServiceListSelect");
        // Initialize the target Rfcomm service
        console.log(serviceList.selectedIndex, chatServices[serviceList.selectedIndex]);
        rfcomm.RfcommDeviceService.fromIdAsync(chatServices[serviceList.selectedIndex].id).done(
            function (service) {
                if (service === null) {
                    console.log(
                        "Access to the device is denied because the application was not granted access",
                        "sample", "status");
                    callback(false);
                    return;
                }

                chatService = service;

                chatService.getSdpRawAttributesAsync(Windows.Devices.Bluetooth.BluetoothCacheMode.uncached).done(
                    function (attributes) {
                        var buffer = attributes.lookup(SDP_SERVICE_NAME_ATTRIBUTE_ID);
                        if (buffer === null) {
                            console.log(
                                "The Chat service is not advertising the Service Name attribute (attribute " +
                                "id=0x100). Please verify that you are running the BluetoothRfcommChat server.",
                                "sample", "error");
                            callback(false);
                            return false;
                        }

                        var attributeReader = streams.DataReader.fromBuffer(buffer);
                        var attributeType = attributeReader.readByte();
                        if (attributeType !== SDP_SERVICE_NAME_ATTRIBUTE_TYPE) {
                            console.log(
                                "The Chat service is using an expected format for the Service Name attribute. " +
                                "Please verify that you are running the BluetoothRfcommChat server.",
                                "sample", "error");
                            callback(false);
                            return false;
                        }

                        var serviceNameLength = attributeReader.readByte();

                        // The Service Name attribute requires UTF-8 encoding.
                        attributeReader.unicodeEncoding = streams.UnicodeEncoding.utf8;
                        serviceName.innerText =
                            "Connected to " + attributeReader.readString(serviceNameLength);

                        chatSocket = new sockets.StreamSocket();
                        chatSocket.connectAsync(
                            chatService.connectionHostName,
                            chatService.connectionServiceName,
                            sockets.SocketProtectionLevel.plainSocket).done(function () {
                                chatWriter = new streams.DataWriter(chatSocket.outputStream);
                                chatBox.style.display = "";

                                receiveStringLoop(new streams.DataReader(chatSocket.inputStream));
                                callback(true);
                            }, function (error) {
                                console.log("Failed to connect to server, with error: " + error, "sample", "error");
                                callback(false);
                                return false;
                            });
                    }, function (error) {
                        console.log("Failed to retrieve SDP attributes, with error: " + error, "sample", "error");
                        callback(false);
                        return false;
                    });
            }, function (error) {
                console.log("Failed to connect to server, with error: " + error, "sample", "error");
                callback(false);
                return false;
            });
        serviceListError.innerHTML = "";
        }

    function receiveStringLoop(reader) {
        /*var iBuffer = reader.readBuffer(length);
        console.log(iBuffer);*/
        var LOAD_ASYNC = 4;
        reader.loadAsync(LOAD_ASYNC).done(function (size) {
            if (size < LOAD_ASYNC) {
                disconnect();
                WinJS.log && WinJS.log("Client disconnected.", "sample", "status");
                return;
            }
            console.log("reader.loadasync4");

            var stringLength = reader.readUInt32();
            console.log("Here's what I found " + stringLength);
            reader.loadAsync(stringLength).done(function (actualStringLength) {
                console.log("Compare " + actualStringLength + " with " + stringLength);
                if (actualStringLength < stringLength) {
                    disconnect();
                    WinJS.log && WinJS.log("Client disconnected.", "sample", "status");
                    return;
                }

                var currentMessage = document.createElement("option");
                console.log("Received some text");
                //var string = reader.readString(actualStringLength);
               /* var bytes = new Uint8Array(actualStringLength);
                reader.readBytes(bytes);
                console.log(bytes);
                console.log(actualStringLength + " bytes");
                //We have raw byte data that we need to make into text

                var crypt = Windows.Security.Cryptography;
                var buffer = crypt.CryptographicBuffer.createFromByteArray(bytes);
                var text = crypt.CryptographicBuffer.convertBinaryToString(
                    crypt.BinaryStringEncoding.utf8, buffer);
                console.log(buffer);
                console.log(text);*/


                var text = reader.readString(actualStringLength);
                console.log(text.length + "<"+actualStringLength+"<<"+stringLength);
                console.log(text);


                //console.log('"' + text + '"');
                //Now that we have raw text, we need to convert into JSON so that it's actually parsable. 

                currentMessage.innerHTML = "From INFLO: " + text;
                console.log(currentMessage.innerText);
                //conversationSelect.appendChild(currentMessage);
                conversationList.innerHTML = currentMessage.innerText + "<br>" + conversationList.innerHTML;

                var message = JSON.parse(text);
                if (message[SENSOR_PROVIDE] !== undefined) {
                    console.log("Looking for a SENSOR_LIST");
                    var response = { "sensor_level": SENSOR_LEVEL_DEVICE, "time": new Date().getTime(), "priority": SENSOR_PRIORITY_LOW };
                    //FC:F8:AE:61:C8:D4
                    response[SENSOR_LIST] = "SampleSensor,"+SENSOR_RAW_TEXT+","+KINECT_COLOR_SENSOR; //This is the device name and the sensors it can return
                    write(JSON.stringify(response));
                }

                // Restart the read for more bytes. We could just call receiveStringLoop() but in the case subsequent
                // read operations complete synchronously we start building up the stack and potentially crash. We use
                // WinJS.Promise.timeout() invoke this function after the stack for current call unwinds.
                WinJS.Promise.timeout().done(function () { return receiveStringLoop(reader); });
            }, function (error) {
                WinJS.log && WinJS.log("Failed to read the message, with error: " + error, "sample", "error");
            });
        }, function (error) {
            WinJS.log && WinJS.log("Failed to read the message size, with error: " + error, "sample", "error");
        });
    }

    var KINECT_COLOR_SENSOR = "102";
    var SENSOR_LIST = "901";
    var SENSOR_PROVIDE = "902";
    var SENSOR_RAW_TEXT = "999";
    var SENSOR_LEVEL_DEVICE = 1;
    var SENSOR_LEVEL_HUB = 2;
    var SENSOR_PRIORITY_LOW = 11;
    var SENSOR_PRIORITY_MEDIUM = 12;
    var SENSOR_PRIORITY_HIGH = 13;
    function write(msg) {
        try {
            chatWriter.writeUInt32(msg.length);
            chatWriter.writeString(msg);

            chatWriter.storeAsync().done(function () {
                var sentMessage = document.createElement("option");
                sentMessage.innerText = "Sent: " + msg;
                //conversationSelect.appendChild(sentMessage);
                conversationList.innerHTML = sentMessage.innerText + "<br>" + conversationList.innerHTML;
                messageInput.value = "";
            }, function (error) {
                console.log("Error storing async");
                console.log(error);
                WinJS.log && WinJS.log("Failed to send the message to the server, error: " + error,
                    "sample", "error");
            });
        } catch (error) {
            console.log(error);
        }
    }
    function onSendButtonClick() {
        try {
            var msg = messageInput.value;
            var payloadJson = { "sensor_level": SENSOR_LEVEL_DEVICE, "time": new Date().getTime(), "priority": SENSOR_PRIORITY_LOW};
            payloadJson[SENSOR_RAW_TEXT] = msg;
            var payloadString = JSON.stringify(payloadJson);
            write(payloadString);
            
        } catch (error) {
            console.log("Error send button click");
            console.log(error);
            WinJS.log && WinJS.log("Sending message failed with error: " + error);
        }
    }
    function onSendPhotoButtonClick() {
        getPotholeFolder(function (folder) {
            console.log("Response " + folder.name);
            var filesInFolderPromise = folder.getFilesAsync();
            filesInFolderPromise.done(function getFilesSuccess(filesInFolder) {

                // Iterate over the results and print the list of files
                // to the Visual Studio Output window.
                filesInFolder.forEach(function forEachFile(file) {
                    console.log(file.name, file.dateCreated);
                    getRawBytes(file, function(bytes) {
                        var payloadJson = { "sensor_level": SENSOR_LEVEL_DEVICE, "time": new Date().getTime(), "priority": SENSOR_PRIORITY_HIGH};
                        payloadJson[KINECT_COLOR_SENSOR] = bytes;
                        console.log("Sending image bytes");
                        var payloadString = JSON.stringify(payloadJson);
                        write(payloadString);
                    });
                });
            });
        });
    }
    function getRawBytes(storageFile, callback) {
        Windows.Storage.FileIO.readBufferAsync(storageFile).then(function (buffer) {
            /*var bytes = new Uint8Array(buffer.length);
            //console.log(buffer);
            var dataReader = Windows.Storage.Streams.DataReader.fromBuffer(buffer);
            dataReader.readBytes(bytes);
            dataReader.close();
            //console.log(bytes);
            //            callback(bytes.join('').);*/
            var dataReader = Windows.Storage.Streams.DataReader.fromBuffer(buffer);
            var b = dataReader.readBuffer(buffer.length);

            //'data:image/bmp;base64,' + 
            callback( Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(b) );
        });

        /*var reader = storageFile.openSequentialReadAsync();
        reader.done(function (buffer) {
            console.log(buffer);c
            var bytes = new Uint8Array(buffer.length);
            var dataReader = Windows.Storage.Streams.DataReader.fromBuffer(buffer);
            dataReader.readBytes(bytes);
            dataReader.close();
            console.log(bytes);
        });*/
    }
    function getPotholeFolder(callback) {
        var storageFolder = Windows.Storage.StorageFolder;
        var photosFolder = Windows.Storage.KnownFolders.picturesLibrary;
        //console.log(path);
        try {
            //var folderPromise = storageFolder.getFolderFromPathAsync(path);
            var folderPromise = photosFolder.getFoldersAsync();
            console.log("Folder is promised");
        } catch (e) {
            console.error(e);
        }
        var POTHOLE_FOLDER;
        try {
            folderPromise.done(function getFoldersSuccess(subfolders) {
                //console.log(subfolders);
                var folderFound = false;
                for (var i = 0; i < subfolders.length; i++) {
                    var subfolder = subfolders[i];
                    console.log(subfolder.name);
                    if (subfolder.name == "pothole") {
                        folderFound = true;
                        POTHOLE_FOLDER = subfolder;
                        console.log("Found folder already");
                        callback(POTHOLE_FOLDER);
                        return;
                    }
                }
                if (!folderFound) {
                    photosFolder.createFolderAsync("pothole").done(function (folder) {
                        POTHOLE_FOLDER = folder;
                        console.log("POTHOLE created");
                        callback(POTHOLE_FOLDER);
                        return;
                    });
                }
            });
        } catch (e) {
            console.error("But you promised!");
            console.error(e);
        }
    }
    
    function onDisconnectButtonClick() {
        disconnect();
        document.getElementById("runButton").style.display = "";
        WinJS.log && WinJS.log("Disconnected", "sample", "status");
    }

    function disconnect() {
        if (chatWriter) {
            chatWriter.detachStream();
            chatWriter = null;
        }

        if (chatSocket) {
            chatSocket.close();
            chatSocket = null;
        }

        runButton.disabled = false;
        serviceSelector.style.display = "none";
        chatBox.style.display = "none";
        conversationList.innerHTML = "";
    }
})();

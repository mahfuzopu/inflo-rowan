package edu.rowan.coe.inflo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;

import java.lang.reflect.Method;

/**
 * Created by N on 6/26/2014.
 */
// simplified exception handling--should enable OPP and OBEX for PC TO ANDY
public class BluetoothAdapterProxy
{
    public static final int CHANNEL_OPP = 12;

    final BluetoothAdapter target;
    static final Class<?> targetClass = BluetoothAdapter.class;
    Method listenOn;

    public BluetoothAdapterProxy(BluetoothAdapter target)
    {
        this.target = target;
        Class<?>[] args = new Class[] { int.class };
        try
        {
            this.listenOn = targetClass.getDeclaredMethod(
                    "listenUsingRfcommOn", args);
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
        }
    }

    public BluetoothServerSocket listenUsingRfcommOn(int channel)
    {
        try
        {
            return (BluetoothServerSocket) (listenOn.invoke(target,
                    new Object[] { channel }));
        }
        catch (Exception e)
        {
            // complain loud, complain long
            throw new RuntimeException(e);
        }
    }
}
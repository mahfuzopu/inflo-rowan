package edu.rowan.coe.inflo;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by guest1 on 10/13/2015.
 */
public class FusedSensor {
    private ArrayList<SensorDevice> sensorDevices;
    private ArrayList<SensorPayload> nextPayload;
    private String TAG = getClass().getSimpleName();

    public FusedSensor() {
        sensorDevices = new ArrayList<>();
    }
    public void addSensor(SensorDevice device) {
        sensorDevices.add(device);
    }
    public boolean hasDevice(SensorDevice device) {
        return getDevice(device) != null;
    }
    public boolean hasDevice(BluetoothDevice device) {
        return getDevice(device) != null;
    }
    public boolean hasDevice(String address) {
        return getDevice(address) != null;
    }
    public SensorDevice getDevice(SensorDevice device) {
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.equals(device))
                return sensorDevice;
        }
        return null;
    }
    public SensorDevice getDevice(String address) {
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.equalsAddress(address)) {
                return sensorDevice;
            }
        }
        return null;
    }
    public SensorDevice getDevice(BluetoothDevice device) {
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.equalsHardware(device))
                return sensorDevice;
        }
        return null;
    }
    public SensorDevice getDevice(int sensor_id) {
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.hasSensor(sensor_id))
                return sensorDevice;
        }
        return null;
    }
    public boolean hasSensor(int sensor_id) {
        return getDevice(sensor_id) != null;
    }
    public SensorDevice get(int index) {
        return sensorDevices.get(index);
    }

    public SensorDevice[] getSensorsDevices() {
        return sensorDevices.toArray(new SensorDevice[sensorDevices.size()]);
    }
    public String[] getSensorDeviceNames() {
        List<String> names = new LinkedList<>();
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.getSensorTypes() != null) //Only valid sensors
                names.add(sensorDevice.getIdentity());
        }
        return names.toArray(new String[names.size()]);
    }
    public String[] getSensorDeviceNamesOnly() {
        List<String> names = new LinkedList<>();
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.getSensorTypes() != null) //Only valid sensors
                names.add(sensorDevice.getName());
        }
        return names.toArray(new String[names.size()]);
    }
    //Checks all the priorities and determines what is the most significant
    public int getPriority() {
        int priority = SensorPayload.PRIORITY_LOW;
        if(sensorDevices.size() == 0)
            return SensorPayload.PRIORITY_LOW;
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.isConnected() && sensorDevice.getLastPayload() != null) {
                priority = Math.max(SensorPayload.PRIORITY_LOW, sensorDevice.getLastPayload().getPriority());
            }
        }
        return priority;
    }
    public String getSensor(int sensor_id) {
        SensorDevice sensorDevice = getDevice(sensor_id);
        if(sensorDevice == null)
            return null;
        if(sensorDevice.getLastPayload() == null)
            return null;
        if(sensorDevice.getLastPayload().getSensorData() == null)
            return null;
        if(sensorDevice.getLastPayload().getSensorData().get(sensor_id + "") == null)
            return null;
        return sensorDevice.getLastPayload().getSensorData().get(sensor_id+"").toString();
    }
    public String[] toArray() {
        ArrayList<String> devices = new ArrayList<>();
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice instanceof RealSensorDevice)
                devices.add(sensorDevice.toString());
        }
        return devices.toArray(new String[devices.size()]);
    }

    /**
     * Creates a single JSONObject and serializes it based on all data
     * @return
     */
    public String toPayload() {
        if(nextPayload == null) {
            nextPayload = new ArrayList<>();
            nextPayload.add(new SensorPayload());
        }
        JSONArray payloads = new JSONArray();
        Log.d(TAG, "FusedSensor.toPayload");
        Log.d(TAG, sensorDevices.size()+" devices");
        Log.d(TAG, "Iterate? "+nextPayload.size());

        for(SensorPayload payload: nextPayload) {
            for (SensorDevice sensorDevice : sensorDevices) {
                Log.d(TAG, sensorDevice.getName()+" "+(sensorDevice.getLastPayload() != null));
                if (sensorDevice.getLastPayload() != null) {
                    for (String sensor : sensorDevice.getLastPayload().getSensorData().keySet()) {
                        Log.d(TAG, "Sensor "+sensor);
                        payload.put(sensor, sensorDevice.getLastPayload().getSensorData().get(sensor));
                        payload.setPriority(Math.max(SensorPayload.PRIORITY_LOW, sensorDevice.getLastPayload().getPriority()));
                    }
                }
            }
            payloads.put(payload.toJSON());
        }
        return payloads.toString();
    }

    public void setConnected(String mac, boolean connected) {
        for(SensorDevice sensorDevice: sensorDevices) {
            if(sensorDevice.equalsAddress(mac))
                sensorDevice.setConnected(connected);
        }
    }

    public void clear() {
        for(SensorDevice sensorDevice: sensorDevices) {
            sensorDevice.clear();
        }
    }

    public void removeSensor(SensorDevice device) {
        sensorDevices.remove(device);
    }
}

package edu.rowan.coe.inflo;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by N on 6/13/2014.
 */
public class MessageAdapter extends ArrayAdapter<MessageItem> {
    private ArrayList<MessageItem> items;
    public MessageAdapter(Context context, int resource) {
        super(context, resource);
        items = new ArrayList<MessageItem>();
    }

    @Override
    public View getView(int position, View currentView, ViewGroup vg) {
        if (currentView == null) {
            currentView = LayoutInflater.from(getContext()).inflate(R.layout.message, null, false);
        }
        if(position >= items.size())
            return currentView;
        LinearLayout v = (LinearLayout) currentView;
        ((TextView)(v.findViewById(R.id.message_txt))).setText(items.get(position).getText());
        ((TextView)(v.findViewById(R.id.message_auth))).setText(items.get(position).getFrom()+"\n"+items.get(position).getTime());
        if(items.get(position).hasImage()) {
            v.findViewById(R.id.message_img).setVisibility(View.VISIBLE);
            ((ImageView) (v.findViewById(R.id.message_img))).setImageBitmap(items.get(position).getImg());
        } else
            v.findViewById(R.id.message_img).setVisibility(View.GONE);
        return v;
    }
    public void addMessage(MessageItem msg) {
        items.add(msg);
        this.add(msg);
    }
}

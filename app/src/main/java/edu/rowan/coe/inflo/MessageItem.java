package edu.rowan.coe.inflo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Log;
import android.widget.ImageView;

import java.util.Date;

/**
 * Created by N on 6/13/2014.
 */
public class MessageItem {
    private String text;
    private Bitmap img;
    private String from;
    private long timestamp;
    public MessageItem(String t) {
        text = t;
        img = null;
        timestamp = new Date().getTime();
    }
    public MessageItem(String t, byte[] i) {
        text = t;
        img = BitmapFactory.decodeByteArray(i, 0, i.length);
        timestamp = new Date().getTime();
    }
    public MessageItem(String t, String f) {
        text = t;
        from = f;
        timestamp = new Date().getTime();
    }
    public MessageItem(String t, byte[] i, String f) {
        text = t;
        img = BitmapFactory.decodeByteArray(i, 0, i.length);
        from = f;
//        Log.d("mi", (img != null)+""+hasImage());
//        Log.d("message_got", t);
        timestamp = new Date().getTime();
    }
    public boolean hasImage() {
        return (img != null);
    }
    public String getText() {
        return text;
    }
    public Bitmap getImg() {
        return img;
    }
    public String getFrom() {
        return from;
    }
    public String getTime() { return ""+timestamp; }
    public String toString() {
        return "From "+from+": "+text+" with attachement as "+hasImage();
    }
}

package edu.rowan.coe.inflo;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import edu.rowan.coe.inflo.server.MessageTypes;

/**
 * Created by mdmhafi on 7/7/2016.
 */
public class MyLocationListener implements LocationListener {

    Context context;
    private String TAG = "MyLocationListener";

    Handler mHandler;
    public MyLocationListener(Context applicationContext, Handler mHander) {
        this.context = applicationContext;
        this.mHandler  = mHander;
    }

    private void SendMessage(int type,String message)
    {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(type);
        Bundle bundle = new Bundle();
        bundle.putString(MessageTypes.DATA, message);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    @Override
    public void onLocationChanged(Location loc) {
        this.context = context;
      //  editLocation.setText("");
      //  pb.setVisibility(View.INVISIBLE);
        Toast.makeText(
                this.context,
                "Location changed: Lat: " + loc.getLatitude() + " Lng: "
                        + loc.getLongitude(), Toast.LENGTH_SHORT).show();
        String longitude = "Longitude: " + loc.getLongitude();
        Log.v(TAG, longitude);
        String latitude = "Latitude: " + loc.getLatitude();
        Log.v(TAG, latitude);
        String speed = "Speed: " + loc.getSpeed();
        Log.v(TAG, speed);
        String accuracy = "Accuracy: " + loc.getAccuracy();
        Log.v(TAG, accuracy);

        /*------- To get city name from coordinates -------- */
        String cityName = null;
        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(loc.getLatitude(),
                    loc.getLongitude(), 1);
            if (addresses.size() > 0) {
                System.out.println(addresses.get(0).getLocality());
                cityName = addresses.get(0).getLocality();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
                + cityName;
        String data = loc.getLongitude() + "," + loc.getLatitude() + "," + loc.getAccuracy() + "," + loc.getSpeed();

        Log.d(TAG,s);
        Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
       // editLocation.setText(s);
        SendMessage(MessageTypes.MOBILE_GPS_LOCATION,data);
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    /**
     * @return the last know best location
     */
    /*
    private Location getLastBestLocation() {
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if ( 0 < GPSLocationTime - NetLocationTime ) {
            return locationGPS;
        }
        else {
            return locationNet;
        }
    }
    */
}
package edu.rowan.coe.inflo;

import org.joda.time.DateTime;

/**
 * Created by mdmhafi on 6/15/2016.
 */
public class ObuLocation {
    // singletone
    private static ObuLocation instance = null;
    public double longitude , latitude  , temperature = 0, speed = 0, humidity = 0;
    DateTime timestamp;
    public static ObuLocation getInstance()
    {
        if(instance==null)
        {
            instance = new ObuLocation();
            instance.setLatitude(-82.8413570);
            instance.setLongitude(34.6818290);
        }
        return  instance;
    }
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public DateTime getTimestamp() {
        setTimestamp(DateTime.now());
        return timestamp;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Double getSpeed() { return speed; }

    public void setSpeed(double speed) { this.speed = speed; }

    public  void setHumidity(Double hum){ this.humidity = hum;}

    public Double getHumidity() { return this.humidity; }
}

package edu.rowan.coe.inflo;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by guest1 on 12/10/2015.
 */
public class RealSensorDevice extends SensorDevice {
    private String MAC;
    RfcommService service;
    private boolean connected;
    public RealSensorDevice(String name) {
        super(name);
    }
    public RealSensorDevice(String name, String MAC, RfcommService rfcommService) {
        super(name);
        this.MAC = MAC;
        this.service = rfcommService;
//        this.service.start();
    }

    @Override
    public String toString() {
        return "["+((isConnected())?"x":" ")+"]  "+getName()+" "+((MAC.isEmpty())?"":"("+MAC+")");
    }

    public String getMAC() {
        return MAC;
    }

    public boolean isConnected() {
        return connected;
    }

    @Override
    public boolean equalsAddress(String address) {
        if(getMAC() != null && getName() != null) {
            if(getMAC().equals(address) || getName().equals(address))
                return true;
        }
        return false;
    }

    @Override
    public boolean equalsHardware(Object o) {
        BluetoothDevice device = (BluetoothDevice) o;
        if(o instanceof BluetoothDevice) {
            if(getMAC() != null && getMAC().equals(device.getAddress()))
                return true;

        }
        return false;
    }

    @Override
    public void resume() {
        if(getService() != null && getService().getState() == RfcommService.STATE_NONE) {
            getService().start();
        }
    }

    @Override
    public void stop() {
        getService().stop();
    }

    @Override
    public String getIdentity() {
        return getName()+" ("+getMAC()+")";
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
    public void connect() {
        if(getName() != null) //Don't connect to everything
            getService().start();
    }

    public RfcommService getService() {
        return service;
    }

    @Override
    public boolean equals(Object o) {
        return ((RealSensorDevice) o).getMAC().equals(getMAC());
    }
}


package edu.rowan.coe.inflo;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import edu.rowan.coe.inflo.ui.ChatItem;
import edu.rowan.coe.inflo.ui.ChatMessage;
import edu.rowan.coe.inflo.ui.ChatRecyclerView;

@Deprecated
public class RfcommActivity extends AppCompatActivity {
    // Debugging
    private static final String TAG = "Rfcomm";
    private static final boolean D = true;

    // Message types sent from the RfcommService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_READ_IMG = 21;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_WRITE_IMG = 31;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_VIDEO = 6;

    //Message codes
    public static final int MESSAGE_TYPE_TEXT = 1;
    public static final int MESSAGE_TYPE_IMG = 2;
    public static final int MESSAGE_TYPE_VIDEO_URL = 3;

    // Key names received from the RfcommService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // Layout Views
    private TextView mTitle;
    private ListView mConversationView;
    private EditText mOutEditText;
    private Button mSendButton;
    private ScheduledExecutorService executor;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private MessageAdapter mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private RfcommService mChatService = null;

    private boolean currentlyDiscoverable; //Whether other devices can see this phone
    private static final String REPEAT_LOOP = "REPEAT_HANDLER";
    private boolean SHOULD_REPEAT_LOOP = false;

    private static final String REPORT_INCIDENT_EMAIL = "test@example.com";
    private RecyclerView mRecyclerView;

    //We can also use virtual sensors
    private VirtualLocationSensor virtualLocationSensor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");
        loadMainUI();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        virtualLocationSensor = new VirtualLocationSensor(RfcommActivity.this);
    }

    LinearLayoutManager mLayoutManager;
    ChatRecyclerView mAdapter;
    ArrayList<ChatItem> mDataset;
    public void loadMainUI() {
        setContentView(R.layout.activity_chat);
        mTitle = (TextView) findViewById(R.id.title_bar);
        mTitle.setText(R.string.app_name);
        mRecyclerView = (RecyclerView) findViewById(R.id.chat_recycler);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        refreshAdapter();
    }

    public void refreshAdapter() {
        // specify an adapter (see also next example)
        if(mDataset == null)
            mDataset = new ArrayList<>();
        mAdapter = new ChatRecyclerView(mDataset);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mChatService == null) setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == RfcommService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
        for(SensorDevice sensorDevice: sensors.getSensorsDevices()) {
            sensorDevice.resume();
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the array adapter for the conversation thread
//        mConversationArrayAdapter = new MessageAdapter(this, R.layout.message);
//        mConversationView = (ListView) findViewById(R.id.in);
//        mConversationView.setAdapter(mConversationArrayAdapter);

        // Initialize the compose field with a listener for the return key
        mOutEditText = (EditText) findViewById(R.id.edit_text_out);
        mOutEditText.setOnEditorActionListener(mWriteListener);

        // Initialize the send button with a listener that for click events
        mSendButton = (Button) findViewById(R.id.button_send);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                TextView view = (TextView) findViewById(R.id.edit_text_out);
                String message = view.getText().toString();
                sendMessage(message);
            }
        });

        // Initialize the RfcommService to perform bluetooth connections
        mChatService = new RfcommService(this, mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");

        listenForDevices();

        //Start discovering service
        loop.sendEmptyMessage(0);
    }

    Handler loop = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            listenForDevices();
            if(!currentlyDiscoverable)
                ensureDiscoverable();
            if(msg.getData().getBoolean(REPEAT_LOOP) && SHOULD_REPEAT_LOOP)
                sendMessageDelayed(msg, 1000);
        }
    };

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
        virtualLocationSensor.killSensor();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");

        for(SensorDevice sensorDevice: sensors.getSensorsDevices()) {
            sensorDevice.stop();
        }
    }

    private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        sendMessage(message, MESSAGE_TYPE_TEXT);
    }
    private void sendMessage(String message, int code) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != RfcommService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (!message.isEmpty()) {
            // Get the message bytes and tell the RfcommService to write
            SensorPayload newPayload = new SensorPayload();
            newPayload.put(SensorPayload.SENSOR_RAW_TEXT + "", message);
            byte[] data = newPayload.toString().getBytes();

            mChatService.write(data, code);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            mOutEditText.setText(mOutStringBuffer);
        }
    }
    private void sendBytes(byte[] msg) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != RfcommService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        mChatService.write(msg, 2);
        // Reset out string buffer to zero and clear the edit text field
        mOutStringBuffer.setLength(0);
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    // If the action is a key-up event on the return key, send the message
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    if(D) Log.i(TAG, "END onEditorAction");
                    return true;
                }
            };

    // The Handler that gets information back from the RfcommService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d(TAG, "Receive handler command " + msg.what);
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case RfcommService.STATE_CONNECTED:
                            mTitle.setText(R.string.title_connected_to);
                            mTitle.append(" " + mConnectedDeviceName);
                            Log.d(TAG, "Connected to "+mConnectedDeviceName);
                            mDataset = new ArrayList<>();
                            refreshAdapter();
                            break;
                        case RfcommService.STATE_CONNECTING:
                            mTitle.setText(R.string.title_connecting);
                            Log.d(TAG, "Connecting to device");
                            break;
                        case RfcommService.STATE_LISTEN:
                            mTitle.setText("Listening");
                            Log.d(TAG, "Service is listening");
                            Log.d(TAG, "Listens to "+mConnectedDeviceName);
//                            break;
                        case RfcommService.STATE_NONE:
                            mConnectedDeviceName ="";
                            mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    Log.d(TAG, "Write message "+writeMessage);
                    Log.d(TAG, "Sent a text!");
//                    mConversationArrayAdapter.addMessage(new MessageItem(writeMessage, "Me"));
                    mDataset.add(new ChatMessage(writeMessage, "Me", new Date().getTime()));
                    refreshAdapter();
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    SensorPayload payload = new SensorPayload(readMessage);
                    Log.d(TAG, "Received a text!");
                    Log.d(TAG, readMessage);

                    //If it is a SENSOR_LIST, specifically mention this
                    //and update `sensors`
                    if(payload.getSensorData().containsKey(SensorPayload.SENSOR_LIST+"")) {
                        Log.d(TAG, "Update sensors");
                        //FIXME There needs to be an identifier for any given sensor
                        String sensorsList = (String) payload.getSensorData().get(SensorPayload.SENSOR_LIST+"");
                        String[] sensorListArray = sensorsList.split(",");
                        if(sensors.hasDevice(sensorListArray[0])) {
                            sensors.getDevice(sensorListArray[0]).setAvailableSensors(sensorListArray);
                            Log.d(TAG, "Got data for device "+sensors.getDevice(sensorListArray[0]).getName());
                        } else {
                            Log.d(TAG, "Found an entirely new device, so let us add it in");
                            SensorDevice newSensor = new RealSensorDevice(sensorListArray[0]);
                            newSensor.setLastPayload(payload);
                            newSensor.setAvailableSensors(sensorListArray);
                            sensors.addSensor(newSensor);
                        }
                    }
                    //Check the priority
                    int priority = payload.getPriority();
                    if(priority == SensorPayload.PRIORITY_HIGH)
                        Log.d(TAG, "Send this now!");

                    //We have received data from a sensor. Let's store this data for the given device.
                    for(String sensor_id: payload.getSensorData().keySet()) {
                        SensorDevice sensorDevice = sensors.getDevice(Integer.parseInt(sensor_id));
                        if(sensorDevice != null) {
                            sensorDevice.setLastPayload(payload);
                            //And update the sender
                            mConnectedDeviceName = sensorDevice.getName();
                        }
                    }
//                    mConversationArrayAdapter.addMessage(new MessageItem(readMessage, mConnectedDeviceName));
                    mDataset.add(new ChatMessage(readMessage, mConnectedDeviceName, payload.getTimestamp()));
                    refreshAdapter();
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device
                    //This is the MAC, not the name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    sensors.setConnected(mConnectedDeviceName, true);
                    Log.d(TAG, "Received device name: "+mConnectedDeviceName);
                    if(!sensors.hasDevice(mConnectedDeviceName)) {
                        sensors.addSensor(new RealSensorDevice("UntitledSensor", mConnectedDeviceName, new RfcommService(RfcommActivity.this, mHandler)));
                    }
                    Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Log.d(TAG, "Toast received "+msg.getData().getString(TOAST));
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device, secure);
        Log.d(TAG, "We are trying to connect to device "+address);
        Log.d(TAG, "Secure connection is " + secure);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.connect_sensors:
                new MaterialDialog.Builder(RfcommActivity.this)
                        .title("Nearby Devices")
                        .items(sensors.toArray())
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                                connectToDevice(sensors.get(i).getIdentity());
                            }
                        })
                        .negativeText("Cancel")
                        .show();
                return true;
            case R.id.discoverable:
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            case R.id.preview_payload:
                new MaterialDialog.Builder(RfcommActivity.this)
                        .title("Payload to Send")
                        .content(sensors.toPayload())
                        .show();
                return true;
            case R.id.view_sensors:
                new MaterialDialog.Builder(RfcommActivity.this)
                        .title("Currently Connected to")
                        .items(sensors.getSensorDeviceNames())
                        .show();
                return true;
            case R.id.report:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","sample@example.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Incident Report");
                SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, YYYY HH:mm:ss");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "At "+virtualLocationSensor.getLatLong()+" on "+sdf.format(new Date()+" the following occurred: "));
                startActivity(emailIntent);
                return true;
            case R.id.last_image:
                Bitmap immagex = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

                Log.e("LOOK", imageEncoded);

                MaterialDialog md = new MaterialDialog.Builder(RfcommActivity.this)
                        .title("Received this image")
                        .customView(R.layout.image_view, false)
                        .build();
                //Get color photo
                Log.d(TAG, "Getting color photo " + sensors.hasSensor(SensorPayload.KINECT_COLOR_SENSOR));
                if(sensors.hasSensor(SensorPayload.KINECT_COLOR_SENSOR)) {
                    String kinect = sensors.getSensor(SensorPayload.KINECT_COLOR_SENSOR);
                    if(kinect != null) {
                        //Ugh, this is a lot of work -- need to convert String to byte[]
                        /*String[] stringBytes = kinect.split(",");
                        ArrayList<Byte> byteArrayList = new ArrayList<>();
                        for(String s: stringBytes) {
                            byteArrayList.add(Byte.parseByte((Integer.parseInt(s)-128)+""));
                        }
                        Byte[] capitalColorBytes =
                                byteArrayList.toArray(new Byte[byteArrayList.size()]);
                        byte[] colorBytes = new byte[capitalColorBytes.length];
                        for (int i = 0; i < capitalColorBytes.length; i++) {
                            colorBytes[i] = capitalColorBytes[i];
                        }*/
//                        String base64Sample = getString(R.string.base64);
                        byte[] colorBytes = Base64.decode(kinect, Base64.DEFAULT);

                        Bitmap color = BitmapFactory.decodeByteArray(colorBytes, 0, colorBytes.length);
//                        Log.d(TAG, color.getHeight()+"x"+color.getWidth());
                        Log.d(TAG, color.toString());
                        ((ImageView) md.getCustomView().findViewById(R.id.image)).setImageBitmap(color);
                        md.show();
                    } else {
                        new MaterialDialog.Builder(RfcommActivity.this)
                                .title("No Images")
                                .content("You have not sent any images")
                                .show();
                    }
                } else {
                    new MaterialDialog.Builder(RfcommActivity.this)
                            .title("No Images")
                            .content("You have no sensors that can send images")
                            .show();
                }
                return true;
            case R.id.location:
                new MaterialDialog.Builder(RfcommActivity.this)
                        .title("Location Information")
                        .content(virtualLocationSensor.toString())
                        .show();
                return true;
        }
        return false;
    }

    /*
    LISTENFORDEVICES
    LISTEN FOR DEVICES
     */
//    private List<SensorDevice> sensors = new ArrayList<>();
    private FusedSensor sensors = new FusedSensor();
    public void listenForDevices() {
        //This is the device discovery service
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtAdapter.startDiscovery();

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);
    }
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    sensors.addSensor(new RealSensorDevice(device.getName(), device.getAddress(), new RfcommService(RfcommActivity.this, mHandler)));
                } else {
                    if(sensors.hasDevice(device))
                        sensors.getDevice(device).setConnected(true);
                    else {
                        Log.d(TAG, "Bonded to unlisted device");
                        Log.d(TAG, device.getAddress());
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //Don't do anything
            }
        }
    };

    public void connectToDevice(String MAC) {
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(MAC);
        // Attempt to connect to the device
        mChatService.connect(device, false);
    }

    public void sendPayloadToServer() {
        String sensorPayload = sensors.toPayload();
        //TODO Actually send payload
    }
}

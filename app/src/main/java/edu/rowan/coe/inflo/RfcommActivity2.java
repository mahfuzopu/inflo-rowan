package edu.rowan.coe.inflo;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import com.afollestad.materialdialogs.MaterialDialog;

import org.joda.time.DateTime;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;

import edu.rowan.coe.inflo.fragment.Payload;
import edu.rowan.coe.inflo.server.InfloResponse1;
import edu.rowan.coe.inflo.server.InfloServer1;
import edu.rowan.coe.inflo.server.MessageTypes;
import edu.rowan.coe.inflo.server.SocketServer;
import edu.rowan.coe.inflo.ui.GridViewAdapter;
import edu.rowan.coe.inflo.ui.ImageItem;
import edu.rowan.coe.inflo.ui.UploadToServer;
import edu.rowan.coe.inflo.ui.Vehicle;
import edu.rowan.coe.inflo.ui.VehicleLiveData;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class RfcommActivity2 extends AppCompatActivity implements Payload.OnFragmentInteractionListener {

    //this is for future use :



    // Debugging
    private static final String TAG = "RfcommActivity2";
    private static final boolean D = true;

    /* GPS Constant Permission */
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;

    // Message types sent from the RfcommService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_READ_IMG = 21;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_WRITE_IMG = 31;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_VIDEO = 6;
    public static final int MESSAGE_READ_OBU_DATA = 7;

    //Message codes
    public static final int MESSAGE_TYPE_TEXT = 1;
    public static final int MESSAGE_TYPE_IMG = 2;
    public static final int MESSAGE_TYPE_VIDEO_URL = 3;

    // Key names received from the RfcommService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // Layout Views
//    private TextView mTitle;
//    private ListView mConversationView;
//    private EditText mOutEditText;
//    private Button mSendButton;
    private ScheduledExecutorService executor;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private MessageAdapter mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private RfcommService mChatService = null;

    private boolean currentlyDiscoverable; //Whether other devices can see this phone
    private static final String REPEAT_LOOP = "REPEAT_HANDLER";
    private boolean SHOULD_REPEAT_LOOP = false;

    private static final String REPORT_INCIDENT_EMAIL = "test@example.com";
//    private RecyclerView mRecyclerView;

    //We can also use virtual sensors
    private VirtualLocationSensor virtualLocationSensor;
    private Payload payload;
    private int LOOP_SPEED = 10*1000;

    private String baseUrl = "http://felkerdigitalmedia.com/inflo/";
    private InfloServer1 infloServer;

    private GridView gridView;
    private GridViewAdapter gridAdapter;

    Bitmap uplodImage;
    String uplodImagefileName;
    Button btUploadToServer ;
    ArrayList<ImageItem> gridImageItems;

    private TextView txOBULocation,txPotholeTempC;
    private TextView txLongitude, txLatitude , txSpeed ,txSensorTemp, txAirTemp;

    SocketServer socketServer = null ;

    private String mProviderName;
    LocationManager locationManager ;

    public static final File POTHOLE_FILE_PATH = new File(android.os.Environment.getExternalStorageDirectory(),"POTHOLE");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");
        loadMainUI();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        virtualLocationSensor = new VirtualLocationSensor(RfcommActivity2.this);

        Retrofit client = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        infloServer = client.create(InfloServer1.class);

        //gridView = (GridView)findViewById(R.id.gridView);
        //loadImages();

        //this bitmap will be uploaded to server. the first image. for demo only.



        //txOBULocation = (TextView)findViewById(R.id.obu_location);
//        txOBULocation.setText("OBU Location:" + "--,--");
        //txPotholeTempC = (TextView)findViewById(R.id.sensor_temp);
        txLongitude = (TextView)findViewById(R.id.tx_longitude);
        txLatitude = (TextView)findViewById(R.id.tx_latitude);
        txSpeed = (TextView)findViewById(R.id.tx_speed);
        txSensorTemp = (TextView)findViewById(R.id.tx_sensorTemp);
        txAirTemp = (TextView)findViewById(R.id.tx_airTemp);

        MobileGPSLocationActivation();


        //this is for ambient temp sensor
        SensorManager mySensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        Sensor AmbientTemperatureSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        Sensor HumiditySensor = mySensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);

        if(HumiditySensor==null)
        {
           // Toast.makeText(RfcommActivity2.this,"Humidity sensor does not exist",Toast.LENGTH_SHORT).show();
            ((TextView)findViewById(R.id.txHumidity)).setText("--");

        }
        else
        {
            mySensorManager.registerListener(
                    TemperatureSensorListener,
                    HumiditySensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        if(AmbientTemperatureSensor==null)
        {
            txAirTemp.setText("--");
        }
        else
        {
            mySensorManager.registerListener(
                    TemperatureSensorListener,
                    AmbientTemperatureSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }


    }
    private void SendMessage(int type,String message)
    {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(type);
        Bundle bundle = new Bundle();
        bundle.putString(MessageTypes.DATA, message);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }
    private void MobileGPSLocationActivation()
    {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        long minTime = 1 * 1000; // Minimum time interval for update in seconds, i.e. 5 seconds.
        long minDistance = 0; // Minimum distance change for update in meters, i.e. 10 meters.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(getProviderName(), minTime, minDistance, locationListener);

        /*
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
            buildAlertMessageNoGps();


        LocationListener locationListener = new MyLocationListener(this.getApplicationContext(),mHandler);

        Criteria criteria = new Criteria();
        mProviderName = manager.getBestProvider(criteria, true);

        Location LastKnownLocation = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if(LastKnownLocation!=null) {
            Log.d(TAG,"GPS provided");
            SendMessage(MessageTypes.MOBILE_GPS_LOCATION, LastKnownLocation.getLongitude() + "," + LastKnownLocation.getLatitude() + "," + LastKnownLocation.getSpeed() + "," + LastKnownLocation.getAccuracy());
        }
        else
        {
            LastKnownLocation = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(LastKnownLocation!=null)
            {
                Log.d(TAG,"Netwok provided");
                SendMessage(MessageTypes.MOBILE_GPS_LOCATION, LastKnownLocation.getLongitude() + "," + LastKnownLocation.getLatitude() + "," + LastKnownLocation.getSpeed() + "," + LastKnownLocation.getAccuracy());
            }
        }

        // API 23: we have to check if ACCESS_FINE_LOCATION and/or ACCESS_COARSE_LOCATION permission are granted
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            // No one provider activated: prompt GPS
            if (mProviderName == null || mProviderName.equals("")) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }

            // At least one provider activated. Get the coordinates
            switch (mProviderName) {
                case "passive":
                    manager.requestLocationUpdates(mProviderName, 5000, 10 , locationListener);
                    Location location = manager.getLastKnownLocation(mProviderName);
                    break;

                case "network":
                    break;

                case "gps":
                    break;

            }

            // One or both permissions are denied.
        } else {

            // The ACCESS_COARSE_LOCATION is denied, then I request it and manage the result in
            // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSION_ACCESS_COARSE_LOCATION);
            }
            // The ACCESS_FINE_LOCATION is denied, then I request it and manage the result in
            // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_ACCESS_FINE_LOCATION);
            }
        }
        */
    }
    /**
     * Get provider name.
     * @return Name of best suiting provider.
     * */
    String getProviderName() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(false); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        return locationManager.getBestProvider(criteria, true);
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    public static Context getContext()
    {
        return RfcommActivity2.getContext();
    }
    public void loadImages()
    {
        gridImageItems = getData();
        gridAdapter = new GridViewAdapter(this,R.layout.grid_item_layout,gridImageItems);
        gridView.setAdapter(gridAdapter);
    }
    public void uploadImage(String filename)
    {
        Log.d(TAG,"Looking file "  + filename + " in sd card!" );
        // look for the image with the filename
        ImageItem item = getImage(filename);

        if(item!=null)
        {
            uplodImage = item.getImage();
            uplodImagefileName = filename;
            UploadToServer uploadImageToServer= new UploadToServer(getApplicationContext());
            uploadImageToServer.send(uplodImage,uplodImagefileName);
        }
        else
        {
            Log.d(TAG,"File:"+filename+" not found in sd card" );
        }
    }
    private ImageItem getImage(String filename)
    {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        File[] listFile;
        File file = POTHOLE_FILE_PATH;
        //Log.d(TAG,"FIle path:" + file.getPath());
        if(!file.exists())
            file.mkdirs();
        if(file.isDirectory())
        {
            listFile = file.listFiles();
            for(int i = 0 ; i < listFile.length;i++)
            {
                if(listFile[i].getName().equals(filename))
                {
                    Bitmap bitmap = BitmapFactory.decodeFile(listFile[i].getAbsolutePath());
                    return  new ImageItem(bitmap,listFile[i].getName());
                }
            }
        }
        return null;
    }
    private ArrayList<ImageItem> getData() {

        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        File[] listFile;
        /*
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
            imageItems.add(new ImageItem(bitmap, "Image#" + i));
        }
        */
        File file = POTHOLE_FILE_PATH;
        //Log.d(TAG,"FIle path:" + file.getPath());
        if(!file.exists())
            file.mkdirs();
        if(file.isDirectory())
        {
            listFile = file.listFiles();
            for(int i = 0 ; i < listFile.length;i++)
            {
//                Bitmap bitmap = BitmapFactory.decodeFile(listFile[i].getAbsolutePath());
//                imageItems.add(new ImageItem(bitmap, listFile[i].getName()));
                //Toast.makeText(getApplicationContext(),"sending: " + listFile[i].getName(),Toast.LENGTH_SHORT).show();
                //uploadImage(listFile[i].getName()); //send the image names filename into server.

                //UploadToServer server = new UploadToServer();
                //server.send(data,filename,nameOfImgFolder); //this nameOfImage
            }
        }
        return imageItems;
    }
//  LinearLayoutManager mLayoutManager;
//  ChatRecyclerView mAdapter;
//  ArrayList<ChatItem> mDataset;
    public void loadMainUI() {
        setContentView(R.layout.activity_chat2);
        payload = (Payload) getSupportFragmentManager().findFragmentById(R.id.fragment_payload);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        //setupChat(); //will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
//            if (mChatService == null) setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == RfcommService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
        for(SensorDevice sensorDevice: sensors.getSensorsDevices()) {
            sensorDevice.resume();
        }
        if(!SHOULD_REPEAT_LOOP) {
            SHOULD_REPEAT_LOOP = true;
            loop.sendMessageDelayed(sendLooperCommand(SHOULD_REPEAT_LOOP), 100);
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the RfcommService to perform bluetooth connections
       // mChatService = new RfcommService(this, mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");

//        listenForDevices();

        //Start discovering service
//        sendLooperCommand(true);
    }
    public Message sendLooperCommand(boolean loop) {
        Bundle b = new Bundle();
        b.putBoolean(REPEAT_LOOP, loop);
        Message m = new Message();
        m.setData(b);
        return m;
    }
    int listenForDevicesStep = 0;
    Handler loop = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(listenForDevicesStep == 0) {
                listenForDevices();
            }
            listenForDevicesStep = (listenForDevicesStep+1)%6;

            tapThePayload();
           if(!currentlyDiscoverable)
                ensureDiscoverable();
            if(msg.getData().getBoolean(REPEAT_LOOP) && SHOULD_REPEAT_LOOP)
                sendMessageDelayed(sendLooperCommand(true), LOOP_SPEED);
        }
    };
    public void tapThePayload() {
        //HERE IS WHERE I TAP THE PAYLOAD
        Date d = new Date();
        payload.setDateTime(d);
        if(sensors.hasSensor(SensorPayload.GPS_LATITUDE)) {
            //Use 3rd party sensors
            payload.setLocation(sensors.getSensor(SensorPayload.GPS_LATITUDE), sensors.getSensor(SensorPayload.GPS_LONGITUDE), 0);
        } else if(virtualLocationSensor.isConnected()) {
            //Use VirtualLocationSensor
            //VehicleLiveData.getInstance();
            // LocationFromMobileDevice(virtualLocationSensor.getLatitude(),virtualLocationSensor.getLongitude());
            payload.setLocation(virtualLocationSensor.getLatitude(), virtualLocationSensor.getLongitude(), (int) virtualLocationSensor.getSpeed());
        } else {
            payload.setLocation("0", "0", -1);
        }
        payload.setPayloadData(sensors);
    }
    public void LocationFromMobileDevice(String latitude,String longitude)
    {
        try{
            Double lat = Double.valueOf(latitude);
            Double lng = Double.valueOf(longitude);
            if(lat==0 && lng==0) return;

           // ObuLocation location = ObuLocation.getInstance();
           // location.setLatitude(lat);
           // location.setLongitude(lng);
           // Log.d(TAG,"Location Set to:" + location.getLongitude() + " , " + location.getLatitude());

        }catch (Exception er)
        {

        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");

        virtualLocationSensor.killSensor();
        for(SensorDevice sensorDevice: sensors.getSensorsDevices()) {
            sensorDevice.stop();
        }
    }

    private void ensureDiscoverable() {

        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            //Toast.makeText(.this, "", Toast.LENGTH_SHORT).show();
           // startActivity(discoverableIntent);
        }
    }
    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        sendMessage(message, MESSAGE_TYPE_TEXT);
    }
    private void sendMessage(String message, int code) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != RfcommService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (!message.isEmpty()) {
            // Get the message bytes and tell the RfcommService to write
            SensorPayload newPayload = new SensorPayload();
            newPayload.put(SensorPayload.SENSOR_RAW_TEXT + "", message);
            byte[] data = newPayload.toString().getBytes();

            mChatService.write(data, code);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
        }
    }
    private void sendBytes(byte[] msg) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != RfcommService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        mChatService.write(msg, 2);
        // Reset out string buffer to zero and clear the edit text field
        mOutStringBuffer.setLength(0);
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    // If the action is a key-up event on the return key, send the message
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    if(D) Log.i(TAG, "END onEditorAction");
                    return true;
                }
            };

    // The Handler that gets information back from the RfcommService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String data = "";
            Log.d(TAG, "Receive handler command " + msg.what);
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case RfcommService.STATE_CONNECTED:
                            Log.d(TAG, "Connected to " + mConnectedDeviceName);
                            break;
                        case RfcommService.STATE_CONNECTING:
                            Log.d(TAG, "Connecting to device");
                            break;
                        case RfcommService.STATE_LISTEN:
                            Log.d(TAG, "Service is listening");
                            Log.d(TAG, "Listens to "+mConnectedDeviceName);
//                            break;
                        case RfcommService.STATE_NONE:
                            mConnectedDeviceName ="";
                            Log.w(TAG, "No state, not connected");
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    Log.d(TAG, "Write message " + writeMessage);
                    Log.d(TAG, "Sent a text!");
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    SensorPayload payload = new SensorPayload(readMessage);
                    Log.d(TAG, "Received a text!");
                    Log.d(TAG, readMessage);

                    //If it is a SENSOR_LIST, specifically mention this
                    //and update `sensors`
                    if(payload.getSensorData().containsKey(SensorPayload.SENSOR_LIST+"")) {
                        Log.d(TAG, "Update sensors");
                        //FIXME There needs to be an identifier for any given sensor
                        String sensorsList = (String) payload.getSensorData().get(SensorPayload.SENSOR_LIST+"");
                        String[] sensorListArray = sensorsList.split(",");
                        if(sensors.hasDevice(sensorListArray[0])) {
                            sensors.getDevice(sensorListArray[0]).setAvailableSensors(sensorListArray);
                            Log.d(TAG, "Got data for device "+sensors.getDevice(sensorListArray[0]).getName());
                        } else {
                            Log.d(TAG, "Found an entirely new device, so let us add it in");
                            SensorDevice newSensor = new RealSensorDevice(sensorListArray[0]); //For now we just know what it is based on context
                            newSensor.setLastPayload(payload);
                            if(newSensor instanceof RealSensorDevice)
                                ((RealSensorDevice) newSensor).setConnected(true);
                            newSensor.setAvailableSensors(sensorListArray);
                            sensors.addSensor(newSensor);
                        }
                    }
                    //Check the priority
                    int priority = payload.getPriority();
                    if(priority == SensorPayload.PRIORITY_HIGH)
                        Log.d(TAG, "Send this now!");

                    //We have received data from a sensor. Let's store this data for the given device.
                    for(String sensor_id: payload.getSensorData().keySet()) {
                        RealSensorDevice sensorDevice = (RealSensorDevice) sensors.getDevice(Integer.parseInt(sensor_id));
                        if(sensorDevice != null) {
                            sensorDevice.setConnected(true);
                            sensorDevice.setLastPayload(payload);
                            //And update the sender
                            mConnectedDeviceName = sensorDevice.getName();
                        }
                    }
                    tapThePayload();
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device
                    //This is the MAC, not the name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    sensors.setConnected(mConnectedDeviceName, true);
                    Log.d(TAG, "Received device name: "+mConnectedDeviceName);
                    if(!sensors.hasDevice(mConnectedDeviceName)) {
                       // sensors.addSensor(new RealSensorDevice("UntitledSensor", mConnectedDeviceName, new RfcommService(RfcommActivity2.this, mHandler)));
                    }
                    Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
                    tapThePayload();
                    break;
                case MESSAGE_TOAST:
                    Log.d(TAG, "Toast received "+msg.getData().getString(TOAST));
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_READ_OBU_DATA:
                    byte[] readBufObu = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessageObu = new String(readBufObu, 0, msg.arg1);
                    //  mConversationArrayAdapter.add(readMessage);
                    Log.d("MESSAGE_READ:",readMessageObu);
                    String []tokens = readMessageObu.split(",");
                    if(tokens.length==2)
                    {
                        Log.d("Latitude="  , tokens[0]);
                        Log.d("Longitude=" , tokens[1]);

                        //update current location of obu
                        Double longitude = Double.valueOf(tokens[1]); //for clemson -82.83
                        Double latitude = Double.valueOf(tokens[0]);  //for clemson 34.68

                        //ObuLocation.getInstance().setLongitude(longitude);
                        //ObuLocation.getInstance().setLatitude(latitude);

                        txOBULocation.setText("OBU Location:" + tokens[1] +"," + tokens[0]);
                    }
                    else
                    {
                        Log.d(TAG,"Broken BT Data!");
                    }

                    break;
                case MessageTypes.OUTPUT:
                    Log.d(TAG,msg.getData().getString(MessageTypes.DATA));
                    break;
                case MessageTypes.POTHOLE_DETECTED:
                    data = msg.getData().getString(MessageTypes.DATA);
                    Toast.makeText(RfcommActivity2.this,"A pothole detected with id: " + data,Toast.LENGTH_LONG).show();
                    break;
                case MessageTypes.IMAGE_RECEIVED:
                    //loadImages(); //show in grid view in ui
                    Toast.makeText(getApplicationContext(),"Pothole Image Received!",Toast.LENGTH_SHORT).show();
                    //uploadImage(msg.getData().getString(MessageTypes.DATA)); //send the image names filename into server.
                    //uploadImage(msg.getData().getString(MessageTypes.DATA)); //send the image names filename into server.
                    break;
                case MessageTypes.MOBILE_GPS_LOCATION:
                    data = msg.getData().getString(MessageTypes.DATA);
                    Log.d(TAG,data);
                    String []mobile_gps_tokens = data.split(",");
                    if(mobile_gps_tokens.length>=2)
                    {
                        Log.d("Latitude="  , mobile_gps_tokens[0]);
                        Log.d("Longitude=" , mobile_gps_tokens[1]);
                        if(mobile_gps_tokens.length==3)
                            Log.d("Speed=", mobile_gps_tokens[2]);
                        if(mobile_gps_tokens.length==3)
                            Log.d("Accuracy=",mobile_gps_tokens[3]);

                        //update current location of obu
                        Double longitude = Double.valueOf(mobile_gps_tokens[1]); //for clemson -82.83
                        Double latitude = Double.valueOf(mobile_gps_tokens[0]);  //for clemson 34.68

                        //ObuLocation.getInstance().setLongitude(longitude);
                        //ObuLocation.getInstance().setLatitude(latitude);
                        //ObuLocation.getInstance().setTimestamp(DateTime.now());

                        txOBULocation.setText("Mobile Location:" + mobile_gps_tokens[1] +"," + mobile_gps_tokens[0]);
                    }
                    else
                    {
                        Log.d(TAG,"Broken Mobile Location Data!");
                    }

                    break;
            }
        }
    };

    // The Handler that gets information back from the BluetoothChatService

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device, secure);
        Log.d(TAG, "We are trying to connect to device "+address);
        Log.d(TAG, "Secure connection is " + secure);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.connect_sensors:
                connectSensor();
                return true;
            case R.id.discoverable:
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            case R.id.preview_payload:
                new MaterialDialog.Builder(RfcommActivity2.this)
                        .title("Payload to Send")
                        .content(sensors.toPayload())
                        .show();
                return true;
            case R.id.view_sensors:
                new MaterialDialog.Builder(RfcommActivity2.this)
                        .title("Currently Connected to")
                        .items(sensors.getSensorDeviceNames())
                        .show();
                return true;
            case R.id.report:
                reportIncident();
                return true;
            case R.id.last_image:
                MaterialDialog md = new MaterialDialog.Builder(RfcommActivity2.this)
                        .title("Received this image")
                        .customView(R.layout.image_view, false)
                        .build();
                //Get color photo
                Log.d(TAG, "Getting color photo " + sensors.hasSensor(SensorPayload.KINECT_COLOR_SENSOR));
                if(sensors.hasSensor(SensorPayload.KINECT_COLOR_SENSOR)) {
                    String kinect = sensors.getSensor(SensorPayload.KINECT_COLOR_SENSOR);
                    if(kinect != null) {
                        //Ugh, this is a lot of work -- need to convert String to byte[]
                        /*String[] stringBytes = kinect.split(",");
                        ArrayList<Byte> byteArrayList = new ArrayList<>();
                        for(String s: stringBytes) {
                            byteArrayList.add(Byte.parseByte((Integer.parseInt(s)-128)+""));
                        }
                        Byte[] capitalColorBytes =
                                byteArrayList.toArray(new Byte[byteArrayList.size()]);
                        byte[] colorBytes = new byte[capitalColorBytes.length];
                        for (int i = 0; i < capitalColorBytes.length; i++) {
                            colorBytes[i] = capitalColorBytes[i];
                        }*/
//                        String base64Sample = getString(R.string.base64);
                        byte[] colorBytes = Base64.decode(kinect, Base64.DEFAULT);

                        Bitmap color = BitmapFactory.decodeByteArray(colorBytes, 0, colorBytes.length);
//                        Log.d(TAG, color.getHeight()+"x"+color.getWidth());
                        Log.d(TAG, color.toString());
                        ((ImageView) md.getCustomView().findViewById(R.id.image)).setImageBitmap(color);
                        md.show();
                    } else {
                        new MaterialDialog.Builder(RfcommActivity2.this)
                                .title("No Images")
                                .content("You have not sent any images")
                                .show();
                    }
                } else {
                    new MaterialDialog.Builder(RfcommActivity2.this)
                            .title("No Images")
                            .content("You have no sensors that can send images")
                            .show();
                }
                return true;
            case R.id.location:
                new MaterialDialog.Builder(RfcommActivity2.this)
                        .title("Location Information")
                        .content(virtualLocationSensor.toString())
                        .show();
                return true;
            case  R.id.start_server:
                updateConfig();
               // loadImages();
                break;
            case  R.id.start_live_data:
                startLiveData();
                break;
            case R.id.signout:
               // Toast.makeText(RfcommActivity2.this,"Sign Out Clicked",Toast.LENGTH_SHORT).show();
                finish();
                Intent myIntent = new Intent(RfcommActivity2.this, LoginActivity.class);
                //myIntent.putExtra("key", value); //Optional parameters
                RfcommActivity2.this.startActivity(myIntent);

                break;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_ACCESS_COARSE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                } else {
                    // permission denied
                }
                break;
            }
            case MY_PERMISSION_ACCESS_FINE_LOCATION: {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted
                    } else {
                        // permission denied
                    }
                    break;
                }
            }
    }

    private void startLiveData() {
        VehicleLiveData vehicleLiveData = VehicleLiveData.getInstance();
    }

    private void updateConfig() {
        Toast.makeText(getApplicationContext(),"server already started!",Toast.LENGTH_SHORT).show();
     //   startActivity(new Intent(this, SettingsActivity.class));
        if(socketServer!=null)
        {
            Toast.makeText(getApplicationContext(),"server already started!",Toast.LENGTH_SHORT).show();
        }
        else
        {
            socketServer = new SocketServer(mHandler);
            Toast.makeText(getApplicationContext(),"server started!",Toast.LENGTH_SHORT).show();
        }
    }
    /*
    LISTENFORDEVICES
    LISTEN FOR DEVICES
     */
//    private List<SensorDevice> sensors = new ArrayList<>();
    private FusedSensor sensors = new FusedSensor();
    public void listenForDevices() {


        //This is the device discovery service
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtAdapter.startDiscovery();

        // Register for broadcasts when a device is discovered
//        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
//        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
//        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//        this.registerReceiver(mReceiver, filter);
    }
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                   // RealSensorDevice sd = new RealSensorDevice(device.getName(), device.getAddress(), new RfcommService(RfcommActivity2.this, mHandler));
                    //sd.connect();
                    if(!sensors.hasDevice(device.getAddress()) && device.getName() != null){} //Prevent dupes like MARIO-PC continually
                      //  sensors.addSensor(sd);
                } else {
                    if(sensors.hasDevice(device))
                        sensors.getDevice(device).setConnected(true);
                    else {
                        Log.d(TAG, "Bonded to unlisted device");
                        Log.d(TAG, device.getAddress());
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //Don't do anything
            }
        }
    };

    public void connectToDevice(String MAC) {
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(MAC);
        // Attempt to connect to the device
        mChatService.connect(device, false);
    }

    @Override
    public void reportIncident() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","sample@example.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Incident Report");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy hh:mm:ss");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "At "+virtualLocationSensor.getLatLong()+" on "+sdf.format(new Date())+" the following occurred: ");
        startActivity(emailIntent);
    }
    void showPairedDevices(){
        final Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices == null || pairedDevices.size() == 0) {
            //showToast("No Paired Devices Found");
        } else {

            List<String> listItems = new ArrayList<String>();

            for(BluetoothDevice device : pairedDevices)
            {
                listItems.add(device.getName());
                Log.d(" getPairedDevices()", device.getName());
            }

            final CharSequence[] btDeviceList = listItems.toArray(new CharSequence[listItems.size()]);

            AlertDialog.Builder builder = new AlertDialog.Builder(RfcommActivity2.this);
            builder.setTitle("Connect to a Bluetooth Device.");
            builder.setItems(btDeviceList,new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(RfcommActivity2.this,"Selected device : " + btDeviceList[i] , Toast.LENGTH_SHORT).show();
                    for(BluetoothDevice device: pairedDevices){
                        String d = btDeviceList[i].toString().trim();
                        if(device.getName().contains(d)){
                            connect(device);
                            break;
                        }
                    }
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
    @Override
    public void connectSensor() {
        Log.d(TAG,"in connect sensor function");
        Toast.makeText(RfcommActivity2.this, "clicked on connect sensors!", Toast.LENGTH_SHORT).show();
        showPairedDevices();

        /*
        new MaterialDialog.Builder(RfcommActivity2.this)
                .title("Nearby Devices")
                .items(sensors.toArray())
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                        connectToDevice(((RealSensorDevice) sensors.get(i)).getMAC());
                    }
                })
                .negativeText("Cancel")
                .show();
         */
    }

    @Override
    public void sendData() {
        sendPayloadToServer();
        Toast.makeText(RfcommActivity2.this, "Uploading payload...", Toast.LENGTH_SHORT).show();
        sensors.clear();
        tapThePayload();
    }

    public void sendPayloadToServer() {
        if(!sensors.hasSensor(SensorPayload.GPS_LONGITUDE)) {
            sensors.addSensor(virtualLocationSensor);
            Log.d(TAG, "No GPS sensor, so I'm using the VLS.");
            Log.d(TAG, virtualLocationSensor.getLastPayload().toString());
        }
        String sensorPayload = sensors.toPayload();

        infloServer.send(sensorPayload).enqueue(new Callback<InfloResponse1>() {
            @Override
            public void onResponse(Response<InfloResponse1> response, Retrofit retrofit) {
                String message = response.message();
                Toast.makeText(RfcommActivity2.this, message, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "INFLO SensorServer: "+message);
            }
            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                Toast.makeText(RfcommActivity2.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
//            Toast.makeText(getApplicationContext(),"lng:" + location.getLongitude() + ", lat" + location.getLatitude() + ", acc:"+ location.getAccuracy(),Toast.LENGTH_SHORT).show();
            double longitude = location.getLongitude();
            double latitude  = location.getLatitude();
            double speed = location.getSpeed() * 2.23694;

            ObuLocation.getInstance().setLongitude(longitude);
            ObuLocation.getInstance().setLatitude(latitude);
            ObuLocation.getInstance().setSpeed(speed);

            String output = "Longitude:\t"+longitude+"\nLatitude:\t"+latitude+"\nSpeed:\t"+location.getSpeed()+" mph";
            //((TextView)findViewById(R.id.location)).setText(output);
            txLongitude.setText(""+ String.format("%.4f",longitude));
            txLatitude.setText("" + String.format("%.4f",latitude));
            txSpeed.setText("" +String.format("%.2f",location.getSpeed()));

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    ////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////// This part is for Bluetooth Connection /////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    public ConnectThread mConnectThread  = null ;
    public ConnectedThread mConnectedThread = null ;
    public synchronized void connect(BluetoothDevice device){
        Log.d("connect function", "connect to device :" + device);

        if(mConnectThread!=null){
            mConnectThread.cancel();
            mConnectThread = null ;
        }

        if(mConnectedThread!=null){
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
    }
    public void connected(BluetoothDevice mmDevice, BluetoothSocket mmSocket) {
        // TODO Auto-generated method stub
        Log.d("Func: Connected", "Device Connected!");

		/* now close all the connection and start the data communication thread */

        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.isConnectedThreaRunning = true ;
        mConnectedThread.start();

    }
    /* thread for making connection */
    public class ConnectThread extends Thread{

        private final BluetoothDevice mmDevice ;
        private final BluetoothSocket mmSocket ;

        public ConnectThread(BluetoothDevice device){
            this.mmDevice = device ;
            BluetoothSocket tmp = null ;
            try{

                //tmp = mmDevice.createRfcommSocketToServiceRecord(MY_UUID2);
                Method m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
                tmp = (BluetoothSocket) m.invoke(device, 1);

            }catch(Exception e){
                Log.e("ConnectThread", "create() failed", e);
            }
            mmSocket = tmp ;
        }
        @Override
        public void run() {
            // TODO Auto-generated method stub
            //mBluetoothAdapter.cancelDiscovery();
            Log.i("ConnectThread", "BEGIN mConnectThread");
            try{
                mmSocket.connect();

            }catch(Exception e){
                Log.e("ConnectThread", "connection() failed", e);
                showToast("Connection Failed.");
                try{
                    mmSocket.close();
                }catch(Exception ee){
                    Log.e("ConnectThread", "closed() failed", e);
                }

                return ;
            }
            connected(mmDevice,mmSocket);
        }
        public void cancel() {
            try {
                mmSocket.close();
            } catch (Exception e) {
                Log.e("ConnectThread", "close() of connect socket failed", e);
            }
        }

    }
    /* thread for maintaining connection */

    /*
     * This thread is called after a successful connection to bluetooth device.
     */
    public class ConnectedThread extends Thread {

        public  boolean isConnectedThreaRunning ;
        private final  BluetoothSocket mmSocket ;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket){
            Log.d("ConnectedThread", "create ConnectedThread");
            mmSocket = socket ;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try{
                tmpIn 	= socket.getInputStream();
                tmpOut 	= socket.getOutputStream();

                showToast("Connected!");



            }catch(Exception e){
                Log.d("ConnectedThread", "Failed to init input output stream!");

            }
            mmInStream 	= tmpIn ;
            mmOutStream = tmpOut;
        }



        @Override
        public void run() {
            Log.i("ConnectedThread", "BEGIN mConnectedThread");
            int bytes;
            while (isConnectedThreaRunning) {
                try {
                    // Read from the InputStream
                    if(mmInStream.available()>0){
                        Thread.sleep(40);
                        byte[] buffer = new byte[1024];

                        bytes = mmInStream.read(buffer);
                        if(bytes>0){
                            Log.d("SIZE",""+bytes);
                            String data = new String(buffer);
                            //Log.d("BT data Received:","Size:" + bytes +  " \\ Data:" + data.trim());
                            //setDataReveddText(data.trim());
                            ParseTempData(data.trim());
                        }
                    }
                } catch (Exception e) {
                    Log.e("ConnectedThread", "disconnected", e);
                    cancel();
                    break;
                }
            }

        }
        public void write(String string) {
            try{
                mmOutStream.write(string.getBytes());
            }catch(Exception e){
                Log.e("ConnectedThread", "write failed", e);
            }

        }
        public void cancel() {
            // TODO Auto-generated method stub
            try{
                mmSocket.close();
                isConnectedThreaRunning = false ;
            }catch(Exception e){
                Log.e("Fuction:ConnedtedThread","Cancel",e);
            }
        }
    }
    void showToast(String str)
    {
//        Toast.makeText(RfcommActivity2.this,str,Toast.LENGTH_SHORT).show();
    }
    void ParseTempData(String data)
    {
        String split[] = data.split(": ");
        if(split.length>1)
        {
            //Log.d()split[1].trim();
            Log.d("BT data Received:","Tempareture==" + split[1].trim());
            if(split[0].contains("Cel") || split[0].contains("Temp"))
            {
                try {
                    double dd = 32.0+ (Double.parseDouble(split[1].trim()) * ( 9.0/5.0));
                    ObuLocation.getInstance().setTemperature(dd);
                    final double  temp = dd;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // txPotholeTempC.setText(" " + temp  + " degF");
                            txSensorTemp.setText(String.format("%.2f",temp));
                        }
                    });

                } catch (Exception ee) {
                    Log.e(TAG, "error in converting temperature!");
                }
            }
            else if(split[0].contains("Hum")){
                try{
                    double dd = Double.parseDouble(split[1].trim());
                    final double humidity = dd;
                    ObuLocation.getInstance().setHumidity(dd);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((TextView)findViewById(R.id.txHumidity)).setText(String.format("%2f",humidity));
                        }
                    });
                }catch (Exception ex){

                }
            }
        }
    }


    /// Ambient temp sensor listener
    private final SensorEventListener TemperatureSensorListener = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
                txAirTemp.setText(String.format("%.2f",32.0+ (9.0/5.0)*(event.values[0])));
            }
            else  if(event.sensor.getType()== Sensor.TYPE_RELATIVE_HUMIDITY){
                //((TextView)findViewById(R.id.txHumidity)).setText(String.format("%.2f",event.values[0]));
            }
        }

    };

    private final SensorEventListener AmbientTemperatureSensorListener = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
                //textAMBIENT_TEMPERATURE_reading.setText("AMBIENT TEMPERATURE: " + event.values[0]);
                txAirTemp.setText(String.format("%.2f",event.values[0]));
            }
        }

    };


}
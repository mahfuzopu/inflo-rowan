package edu.rowan.coe.inflo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
public class RfcommService {
    // Debugging
    private static final String TAG = "RfcommService";
    private static final boolean D = true;

    // Name for the SDP record when creating server socket
    private static final String NAME_INSECURE = "INFLO";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");
//            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    private static final UUID MY_UUID = UUID.fromString("66841278-c3d1-11df-ab31-001de000a901");

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread mSecureAcceptThread;
    private AcceptThread mInsecureAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    /**
     * Constructor. Prepares a new Rfcomm session.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     */
    public RfcommService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the chat connection
     * @param state  An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(RfcommActivity.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state. */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume() */
    public synchronized void start() {
        if (D) Log.d(TAG, "start");

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        setState(STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        if (mSecureAcceptThread == null) {
           // mSecureAcceptThread = new AcceptThread(true);
           // mSecureAcceptThread.start();
        }
        if (mInsecureAcceptThread == null) {
           // mInsecureAcceptThread = new AcceptThread(false);
           // mInsecureAcceptThread.start();
        }
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * @param device  The BluetoothDevice to connect
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    public synchronized void connect(BluetoothDevice device, boolean secure) {
        if (D) Log.d(TAG, "connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device, secure);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     * @param socket  The BluetoothSocket on which the connection was made
     * @param device  The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice
            device, final String socketType) {
        if (D) Log.d(TAG, "connected, Socket Type:" + socketType);

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        // Cancel the accept thread because we only want to connect to one device
        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }
        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }

        mConnectedThread = new ConnectedThread(socket, socketType);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(RfcommActivity2.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(RfcommActivity2.DEVICE_NAME, device.getAddress());
        Log.d(TAG, "Send message DEVICE_NAME "+device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);

        // Start the thread to manage the connection and perform transmissions

        //Send a SENSOR_PROVIDE command
       // mConnectedThread.write(sendSensorProvide().toString().getBytes());
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (D) Log.d(TAG, "stop");

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }
        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     */
    public void write(byte[] out, int code) {
        // Create temporary object
        String type = "";
        ConnectedThread r;
        Log.d(TAG, "Write to the ConnectedThread in an unsynchronized manner");
        Log.d(TAG, code+", "+out);
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Send a failure message back to the Activity
        Log.d(TAG, "Connection attempt was unsuccessful");
        Message msg = mHandler.obtainMessage(RfcommActivity.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(RfcommActivity.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        RfcommService.this.start();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // Send a failure message back to the Activity
        Log.d(TAG, "Device connection was lost");
        Message msg = mHandler.obtainMessage(RfcommActivity.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(RfcommActivity.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        RfcommService.this.start();
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;
        private String mSocketType;

        public AcceptThread(boolean secure) {
            BluetoothServerSocket tmp = null;
            mSocketType = secure ? "Secure":"Insecure";

            // Create a new listening server socket
            try {
               // tmp = mAdapter.listenUsingRfcommWithServiceRecord(
               //         NAME_INSECURE, MY_UUID_INSECURE);
                tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_INSECURE, MY_UUID);

            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + " listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            if (D) Log.d(TAG, "Socket Type: " + mSocketType +
                    "  BEGIN mAcceptThread" + this.getName());
            setName("AcceptThread" + mSocketType);

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    if(mmServerSocket != null) {
                      //  socket = mmServerSocket.accept();
                    }
                } catch (Exception e) {
                    Log.w(TAG, "Socket Type:  " + mSocketType + "  accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (RfcommService.this) {
                        Log.d(TAG, "Socket state "+mState);
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice(),
                                        mSocketType);
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(TAG, "Could not close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }
            if (D) Log.i(TAG, "END mAcceptThread, socket Type: " + mSocketType);

        }

        public void cancel() {
            if (D) Log.d(TAG, "Socket Type " + mSocketType + " cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Socket Type " + mSocketType + " close() of server failed", e);
            }
        }
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device, boolean secure) {
            Log.d(TAG, "Initiating new "+secure+" connectthread");
            mmDevice = device;
            BluetoothSocket tmp = null;
            mSocketType = secure ? "Secure" : "Insecure";

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                if (secure) {
                   // tmp = device.createRfcommSocketToServiceRecord(
                   //         MY_UUID_SECURE);
                    tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                } else {
                   // tmp = device.createRfcommSocketToServiceRecord(
                   //         MY_UUID_INSECURE);
                    tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                }
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + " create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread SocketType: " + mSocketType);
            setName("ConnectThread " + mSocketType);

            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                // Close the socket
                Log.d(TAG, e.getMessage()+"<");
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() " + mSocketType +
                            " socket during connection failure", e2);
                    Log.d(TAG, e2.getMessage()+"<<");
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (RfcommService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice, mSocketType);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final DataInputStream mmInStream;
        private final DataOutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket, String socketType) {
            Log.d(TAG, "create ConnectedThread: " + socketType);
            mmSocket = socket;
            DataInputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = new DataInputStream(socket.getInputStream());
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = new DataOutputStream(tmpOut);
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
            byte[] buffer = new byte[1048576];
            int bytes;
            // Keep listening to the InputStream while connected
            while (true) {
                Log.d(TAG, "listening to InputStream");
                try {
                    // Read from the InputStream
//                    Log.d(TAG, "Try to start reading");
//                    byte[] messageSizeBuffer = new byte[512];
//                    Log.d(TAG, "Created message size buffer");
//                    int messageSizeBufferSize = mmInStream.read(messageSizeBuffer, 0, 512);
                    bytes = mmInStream.read(buffer);
                    mHandler.obtainMessage(RfcommActivity2.MESSAGE_READ_OBU_DATA,bytes,-1,buffer).sendToTarget();
                    continue;

                    /*
                    int messageSizeBufferSize = mmInStream.readInt();
                    Log.d(TAG, messageSizeBufferSize+"<mSBS");
//                    Log.d(TAG, new String(messageSizeBuffer)+"<mSB");
//                    int messageSize = Integer.parseInt(new String(messageSizeBuffer).substring(0, messageSizeBufferSize));
//                    Log.d(TAG, messageSize+"<mS");
                    int messageCode = RfcommActivity.MESSAGE_TYPE_TEXT;
                    Log.d(TAG, "Run");

                    if(messageSizeBufferSize == 0)
                        continue;
                    Log.d(TAG, "Message of size "+messageSizeBufferSize);
                    bytes = 0;
                    buffer = new byte[messageSizeBufferSize];
                    while(bytes < messageSizeBufferSize) {
                        Log.d(TAG, "Reading bits "+bytes+" for mmInStream");
                        bytes += mmInStream.read(buffer,bytes,messageSizeBufferSize - bytes);
                    }
                    Log.d(TAG, "Read RFCOMM message completely");
                    String message = new String(buffer);
                    Log.d(TAG, "Read "+message);
                    SensorPayload payload = new SensorPayload(message);
                    if(payload.getLevel() == SensorPayload.SENSOR_DEVICE) {
                        Log.d(TAG, "This is a device");
                        mHandler.obtainMessage(RfcommActivity.MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();
                    } else if(payload.getLevel() == SensorPayload.SENSOR_SERVER) {
                        Log.d(TAG, "This is from server");
                        //Something is coming from the server
                    }
                    */
                } catch (IOException e) {
                    Log.d(TAG, "IOEXception occurred with the input stream");
                    Log.e(TAG, "disconnected", e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Write to the connected OutStream.
         * @param buffer  The bytes to write
         */
        public void write(byte[] buffer) {
            int BIG_NUM = 70000; //~70kb
            for(int i=0; i<buffer.length;i+=BIG_NUM)
            {
                int b = ((i+BIG_NUM) < buffer.length) ? BIG_NUM: buffer.length - i;
                try {
                    mmOutStream.writeInt(buffer.length);
                    mmOutStream.write(buffer, i, b);
                    mmOutStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mHandler.obtainMessage(RfcommActivity.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    //Send SENSOR_PROVIDE
    public SensorPayload sendSensorProvide() {
        SensorPayload payload = createSensorPayload();
        payload.put(SensorPayload.SENSOR_PROVIDE+"", 1);
        return payload;
    }
    public SensorPayload createSensorPayload() {
        SensorPayload payload = new SensorPayload();
        return payload;
    }
}
package edu.rowan.coe.inflo;

import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by guest1 on 10/13/2015.
 * A sensor bluetooth device
 */
public abstract class SensorDevice {
    private String name;
    private Integer[] sensorTypes; //A hashmap of sensors, coming from SENSOR_LIST
    private SensorPayload lastPayload;
    private String TAG = getClass().getSimpleName();

    public SensorDevice(String name) {
        this.name = name;
    }

    public Integer[] getSensorTypes() {
        return sensorTypes;
    }
    public boolean hasSensor(int sensor_id) {
        if((sensorTypes != null && sensorTypes.length > 0) || (getLastPayload() != null &&  getLastPayload().getSensorData().containsKey(sensor_id)))
            return Arrays.asList(sensorTypes).contains(sensor_id);
        return false;
    }

    public SensorPayload getLastPayload() {
        return lastPayload;
    }

    public void setAvailableSensors(String[] sensor_list) {
        Log.d(TAG, "Set available sensors");
        //Get all sensors from the SENSOR_LIST
        //The first one is the name, we can ignore that
        name = sensor_list[0];
        List<Integer> sensorList = new LinkedList<>();
        for(int i=1;i<sensor_list.length;i++) {
            String sensor_id = sensor_list[i];
            Log.d(TAG, "For "+name+", "+sensor_id);
            sensorList.add(Integer.valueOf(sensor_id));
        }
        sensorTypes = sensorList.toArray(new Integer[sensorList.size()]);
    }

    public void setLastPayload(SensorPayload newPayload) {
        if (lastPayload == null) {
            this.lastPayload = newPayload;
           return;
        }
        for(String sensor_id: lastPayload.getSensorData().keySet()) {
            if(!newPayload.getSensorData().containsKey(sensor_id)) {
                Log.d(TAG, sensor_id+" is not included");
                newPayload.put(sensor_id, lastPayload.getSensorData().get(sensor_id));
            } else {
                Log.d(TAG, sensor_id+" is included");
            }
        }
        Log.d(TAG, newPayload.getSensorData().keySet().toString()+", "+lastPayload.getSensorData().keySet().toString());
        if(lastPayload.getPriority() > newPayload.getPriority())
            newPayload.setPriority(lastPayload.getPriority());
        this.lastPayload = newPayload;
    }

    public void clear() {
        lastPayload = null;
    }

    public String getName() {
        return name;
    }

    public abstract boolean isConnected();
    public abstract boolean equalsAddress(String address);
    public abstract boolean equalsHardware(Object o);
    public abstract void resume();
    public abstract void stop();
    public void setConnected(boolean isConnected) {}
    public String getIdentity() {
        return getName();
    }
}

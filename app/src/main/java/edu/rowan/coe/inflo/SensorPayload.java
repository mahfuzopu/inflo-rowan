package edu.rowan.coe.inflo;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by guest1 on 10/13/2015.
 */
public class SensorPayload {
    public static final int SENSOR_DEVICE = 1;
    public static final int SENSOR_HUB = 2;
    public static final int SENSOR_SERVER = 3;

    public static final int KINECT_DEPTH_SENSOR = 101;
    public static final int KINECT_COLOR_SENSOR = 102;
    public static final int TEMPERATURE_SENSOR = 111;
    public static final int GPS_LATITUDE = 121;
    public static final int GPS_LONGITUDE = 122;
    public static final int GPS_SPEED = 123;
    public static final int SENSOR_LIST = 901; //Comma-separated list, first item is device name
    public static final int SENSOR_PROVIDE = 902;
    public static final int SENSOR_RAW_TEXT = 999;

    public static final int PRIORITY_LOW = 11;
    public static final int PRIORITY_MEDIUM = 12;
    public static final int PRIORITY_HIGH = 13;

    private int level;
    private long timestamp;
    private int priority;
    private HashMap<String, Object> sensorData;

    private static final String KEY_SENSOR_LEVEL = "sensor_level";
    private static final String KEY_TIMESTAMP = "time";
    private static final String KEY_PRIORITY = "priority";

    private JSONObject payload;

    private static final String TAG = "SensorPayload";

    //Create empty payload to put things
    public SensorPayload() {
        payload = new JSONObject();
        level = 2;
        timestamp = new Date().getTime();
        priority = PRIORITY_LOW;
        sensorData = new HashMap<>();
    }
    public SensorPayload(String message) {
        Log.d(TAG, "Creating new payload");
        sensorData = new HashMap<>();
        try {
            payload = new JSONObject(message);
            Iterator<String> keys = payload.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                Log.d(TAG, key);
                if(key.equals(KEY_SENSOR_LEVEL)) {
                    level = payload.getInt(KEY_SENSOR_LEVEL);
                } else if(key.equals(KEY_TIMESTAMP)) {
                    timestamp = payload.getLong(KEY_TIMESTAMP);
                } else if(key.equals(KEY_PRIORITY)) {
                    priority = payload.getInt(KEY_PRIORITY);
                } else {
                    /*Log.d(TAG, "Inserting " + key);
                    Log.d(TAG, payload.has(key)+"");
                    Log.d(TAG, payload.getString(key));*/
                    sensorData.put(key, payload.get(key));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getLevel() {
        return level;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getPriority() {
        return priority;
    }

    public HashMap<String, Object> getSensorData() {
        return sensorData;
    }

    public void put(String key, Object value) {
        if (key.equals(KEY_SENSOR_LEVEL)) {
            level = (int) value;
        } else if (key.equals(KEY_TIMESTAMP)) {
            timestamp = (long) value;
        } else if (key.equals(KEY_PRIORITY)) {
            priority = (int) value;
        } else {
            sensorData.put(key, value);
        }
    }

    @Override
    public String toString() {
        if(toJSON() == null)
            return "";
        return toJSON().toString();
    }
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put(KEY_SENSOR_LEVEL, level);
            json.put(KEY_TIMESTAMP, timestamp);
            json.put(KEY_PRIORITY, priority);
            Iterator<String> keys = sensorData.keySet().iterator();
            while(keys.hasNext()) {
                String key = keys.next();
                json.put(key, sensorData.get(key));
            }
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}

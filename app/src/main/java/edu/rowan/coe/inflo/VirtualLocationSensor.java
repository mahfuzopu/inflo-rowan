package edu.rowan.coe.inflo;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.Date;

/**
 * If there is not a dedicated GPS sensor attached, the system will default to using
 * the built-in location services native to Android.
 *
 * The virtual sensor checks the location continually and update the location vars.
 * By computing the change in these attribute over a known period of time, the instantaneous speed
 * can be approximated
 * Created by guest1 on 11/17/2015.
 */
public class VirtualLocationSensor extends SensorDevice
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Context mContext;
//    private Location previousLocation;
    private Location currentLocation;
    private long TIME_STEP = 15*1000;
    private boolean running = true;
    private GoogleApiClient gapi;
    private String TAG = "VirtualLocationSensor";
    private Handler recheckLocation = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(gapi.isConnected()) {
                Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(gapi);
                //previousLocation = currentLocation;
                currentLocation = mCurrentLocation;
                if(mCurrentLocation == null)
                    Log.d(TAG, "Something funky is going on with polling");
//                else
//                    Log.d(TAG, "Polled location virtually");
            }
            if(running) {
                sendEmptyMessageDelayed(0, TIME_STEP);
            }
        }
    };

    public VirtualLocationSensor(Context context) {
        super("Virtual Location Sensor");
        this.mContext = context;
        recheckLocation.sendEmptyMessage(0);

        gapi = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        gapi.connect();
    }
    public void killSensor() {
        running = false;
    }
    /*private double getDistance(Location currentLocation, Location lastLocation) {
        //return Math.sqrt(Math.pow(currentLocation.getSpeed())+Math.pow())
    }
    private double distanceToMilesHour(double distance, long time) {
        double weirdUnitSpeed = distance/time;
    }*/
//    public double getSpeed() {
//        return distanceToMilesHour(getDistance(currentLocation, previousLocation), TIME_STEP);
//    }
    public double getSpeed() {
        float metersPerSecond = currentLocation.getSpeed();
        return metersPerSecond*2.23694;
    }

    @Override
    public String toString() {
        if(currentLocation != null) {
            return "My location is " + currentLocation.getLongitude() + ", " + currentLocation.getLatitude() + ", " +
                    "& my speed is " + getSpeed() + " from " + currentLocation.getProvider() + "\n\n" +
                    "Altitude: " + currentLocation.getAltitude() + "m and " + currentLocation.getBearing() + " degrees\n\n" +
                    currentLocation.getAccuracy() + "% accurate";
        } else {
            return "We do not have location information yet. Please standby.";
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    public String getLatLong() {
        if(currentLocation != null)
            return currentLocation.getLatitude()+", "+currentLocation.getLongitude();
        else
            return "< location not found >";

    }
    public String getLatitude() {
        if(currentLocation != null)
            return currentLocation.getLatitude()+"";
        else
            return "-";
    }
    public String getLongitude() {
        if(currentLocation != null)
            return currentLocation.getLongitude()+"";
        else
            return "-";
    }
    public boolean isConnected() {
        return currentLocation != null && gapi.isConnected();
    }

    @Override
    public boolean equalsAddress(String a) {
        return false;
    }

    @Override
    public boolean equalsHardware(Object o) {
        return false;
    }

    @Override
    public void resume() {
        running = true;
        recheckLocation.sendEmptyMessage(0);
    }

    @Override
    public void stop() {
        killSensor();
    }

    @Override
    public SensorPayload getLastPayload() {
        SensorPayload sp = new SensorPayload();
        sp.setPriority(SensorPayload.PRIORITY_LOW);
        if(isConnected()) {
            sp.put(SensorPayload.GPS_LATITUDE+"", getLatitude());
            sp.put(SensorPayload.GPS_LONGITUDE+"", getLongitude());
            sp.put(SensorPayload.GPS_SPEED+"", getSpeed());
        }
        setLastPayload(sp);
        return sp;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof VirtualLocationSensor;
    }

}

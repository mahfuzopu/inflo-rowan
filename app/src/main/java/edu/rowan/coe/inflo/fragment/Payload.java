package edu.rowan.coe.inflo.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import edu.rowan.coe.inflo.FusedSensor;
import edu.rowan.coe.inflo.ObuLocation;
import edu.rowan.coe.inflo.R;
import edu.rowan.coe.inflo.SensorDevice;
import edu.rowan.coe.inflo.SensorPayload;
import edu.rowan.coe.inflo.server.InfloServer1;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * This UI keeps track of all the payload data and displays it in a friendly UI for the user
 */
public class Payload extends Fragment {

    private OnFragmentInteractionListener mListener;
    View payloadView;
    private static final String TAG = "inflo:PayloadFrag";
    public Payload() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View payloadFragment = inflater.inflate(R.layout.fragment_payload, null);
        payloadView = payloadFragment;
        payloadView.findViewById(R.id.report_incident).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.reportIncident();
            }
        });
        payloadView.findViewById(R.id.connect_sensor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.connectSensor();
            }
        });
        payloadView.findViewById(R.id.button_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.sendData();
            }
        });
        setDateTime(new Date());
        return payloadFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void reportIncident();
        void connectSensor();
        void sendData();
    }
    public void setDateTime(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy - h:mma");
        ((TextView) payloadView.findViewById(R.id.date_time)).setText(sdf.format(time));
    }
    public void setLocation(String longitude, String latitude, int speed) {
        String output = "My Location:    "+longitude+", "+latitude+", "+speed+"mph";
        //((TextView) payloadView.findViewById(R.id.location)).setText(output);
        //ObuLocation.getInstance().setLatitude(Double.parseDouble(latitude));
        //ObuLocation.getInstance().setLongitude(Double.parseDouble(longitude));


    }
    public void setPayloadData(FusedSensor sensor) {
        String output = "";
        //Get list of devices
        String connectedTo = "Connected to ";
        for(String sensorDevice: sensor.getSensorDeviceNamesOnly()) {
            connectedTo += sensorDevice+", ";
        }
        if(connectedTo.length() > new String("Connected to ").length()) {
            output += connectedTo.substring(0, connectedTo.length()-2)+"\n";
        } else {
            output += "Not connected to any sensors\n";
        }
        if(sensor.hasSensor(SensorPayload.SENSOR_RAW_TEXT) && sensor.getSensor(SensorPayload.SENSOR_RAW_TEXT) != null) {
            output += "Text: "+sensor.getSensor(SensorPayload.SENSOR_RAW_TEXT);
        }
        if(sensor.hasSensor(SensorPayload.KINECT_COLOR_SENSOR) && sensor.getSensor(SensorPayload.KINECT_COLOR_SENSOR) != null) {
           // ImageView colorimage = ((ImageView) payloadView.findViewById(R.id.sent_image));
            //colorimage.setVisibility(View.VISIBLE);
            //Set data to blob
            String kinect = sensor.getSensor(SensorPayload.KINECT_COLOR_SENSOR);
            if(kinect != null) {
                byte[] colorBytes = Base64.decode(kinect, Base64.DEFAULT);
                Bitmap color = BitmapFactory.decodeByteArray(colorBytes, 0, colorBytes.length);
                Log.d(TAG, color.toString());
                //colorimage.setImageBitmap(color);
            }
        } else {
            //ImageView colorimage = ((ImageView) payloadView.findViewById(R.id.sent_image));
            //colorimage.setVisibility(View.GONE);
        }
        //((TextView) payloadView.findViewById(R.id.sensor_data)).setText(output);
        switch(sensor.getPriority()) {
            case SensorPayload.PRIORITY_HIGH:
                //((TextView) payloadView.findViewById(R.id.priority)).setText("High Priority");
                break;
            case SensorPayload.PRIORITY_MEDIUM:
                //((TextView) payloadView.findViewById(R.id.priority)).setText("Medium Priority");
                break;
            case SensorPayload.PRIORITY_LOW:
                //((TextView) payloadView.findViewById(R.id.priority)).setText("Low Priority");
                break;
        }
    }
}

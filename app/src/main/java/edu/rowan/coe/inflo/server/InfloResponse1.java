package edu.rowan.coe.inflo.server;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Nick on 11/17/2015.
 */
public class InfloResponse1 {
    @SerializedName("response")
    @Expose
    private String response;

    /**
     *
     * @return
     * The response
     */
    public String getResponse() {
        return response;
    }

    /**
     *
     * @param res
     * The response
     */
    public void setResponse(String res) {
        this.response = res;
    }
}

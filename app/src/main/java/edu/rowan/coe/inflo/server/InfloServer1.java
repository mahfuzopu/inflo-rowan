package edu.rowan.coe.inflo.server;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by guest1 on 12/10/2015.
 */
public interface InfloServer1 {
    @FormUrlEncoded
    @POST("put.php")
    Call<InfloResponse1> send(@Field("payload") String payload);

}

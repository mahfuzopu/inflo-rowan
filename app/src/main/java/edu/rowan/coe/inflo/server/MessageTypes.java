package edu.rowan.coe.inflo.server;

import edu.rowan.coe.inflo.ui.Vehicle;

/**
 * Created by mdmhafi on 6/9/2016.
 */
public final class MessageTypes {
    public static final int CLIENT_CONNECTED = 0;
    public static final int OUTPUT = 100;
    public static final int IMAGE_RECEIVED = 101;
    public static final int LIVE_DATA_AVAILABLE = 102;
    public static final int MOBILE_GPS_LOCATION = 103;
    public static final int POTHOLE_DETECTED = 104;
    public static String DATA = "";
    public static Vehicle vehicle=null;
}

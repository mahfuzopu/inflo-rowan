package edu.rowan.coe.inflo.server;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


import org.battelle.inflo.infloui.MainActivity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StreamCorruptedException;
import java.io.StringReader;
import java.net.Socket;

import edu.rowan.coe.inflo.ObuLocation;
import edu.rowan.coe.inflo.RfcommActivity2;
import edu.rowan.coe.inflo.ui.UploadPotholeToServer;
import edu.rowan.coe.inflo.ui.UploadToServer;

/**
 * Created by mdmhafi on 6/6/2016.
 */
public class ServerReadWriteThread implements Runnable {
    private Socket serverSocket;
    private OutputStream outputStream;
    private PrintStream printStream;
    private int threadCount ;
    private InputStream inputStream;
    private static  final String TAG= "ServerReadWriteThread";
    private FileOutputStream fileOutStream;

    Handler mHandler;
    public  ServerReadWriteThread(Socket sc, int count, Handler handler)
    {
        this.threadCount = count;
        this.serverSocket = sc;
        this.mHandler = handler;
        try
        {
            this.outputStream = serverSocket.getOutputStream();
            printStream = new PrintStream(outputStream);
            this.inputStream = serverSocket.getInputStream();
            this.outputStream = serverSocket.getOutputStream();
            Thread thread = new Thread(this);
            thread.start();
        }catch (Exception ex)
        {

        }
    }
    private void SendMessage(int type,String message)
    {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(type);
        Bundle bundle = new Bundle();
        bundle.putString(MessageTypes.DATA, message);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }
    public void WriteToClient(String msg)
    {
        if(printStream!=null)
        {
            printStream.print(msg);
        }
    }
    private void SaveIntoTempSdCard(String fileFullPath)
    {

    }
    @Override
    public void run() {
       // char buffer[] = new char[1080*1920*3];
       // BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));

        //Log.d(TAG,"Log file path:" + threadCount + "=" +MainActivity.APP_FOLDER_PATH + "/" + "pothole.png" );

        int bytecount = 2048;
        byte[] buf = new byte[bytecount];
        BufferedInputStream BuffIN = null;
        FileOutputStream outToFile = null;



        DataInputStream input = new DataInputStream(inputStream);
        DataOutputStream output = new DataOutputStream(outputStream);
        // Create an inputstream for the file data to arrive
        try
        {
            //outToFile = new FileOutputStream(file);
            //InputStream IN = this.serverSocket.getInputStream();
           // BuffIN = new BufferedInputStream(inputStream, bytecount);

           // Log.d(TAG,"Log file path:" + threadCount + "="+ BuffIN.toString() );

        }catch (Exception ex){
            Log.d(TAG,ex.toString());
            return;
        }

        int count = 1 ;
        while (SocketServer.isServerRunning)
        {
            if(inputStream!=null)
            {
                try {

                    /* data reading sequence
                    * 1. pothole Id (int)
                    * 2. # of image (int)
                    * 3. Longitude (Double)
                    * 4. Latitude (Double)
                    * 5. Speed (int)
                    * 6. Accuracy (int)
                    * 7. Timestamp
                    * */
                    byte[] data ;

                    int PotholeID = input.readInt();
                    int numberOfImg = input.readInt();
                    double imgLongitude = input.readDouble();
                    double imgLatitude = input.readDouble();
                    int vSpeed  = input.readInt();
                    int vAccuracy = input.readInt();

                    int timestamp = input.readInt();


                    Log.d(TAG,"potholeId = " + PotholeID);
                    Log.d(TAG,"number of image = " + numberOfImg);
                    Log.d(TAG,"Longitude=" + imgLongitude);
                    Log.d(TAG,"Latitude=" + imgLatitude);
                    Log.d(TAG,"Speed=" + vSpeed);
                    Log.d(TAG,"Accuracy=" + vAccuracy);


                    if(numberOfImg>0)
                    {
                        //here make a http call to update pothole info
                        UploadPotholeToServer potholeToServer = new UploadPotholeToServer();
                        potholeToServer.upload(PotholeID,
                                "NJDOT001",
                                ObuLocation.getInstance().getLatitude(),  // change it to imgLongitude to use the gps location from matlab.
                                ObuLocation.getInstance().getLongitude(), // same change as before
                                //ObuLocation.getInstance().getTimestamp(), // same change.
                                timestamp,
                                ObuLocation.getInstance().getSpeed(), //speed .. change
                                (double)vAccuracy,  //accuracy change.
                                ObuLocation.getInstance().getTemperature(),
                                ObuLocation.getInstance().getHumidity()); //temperature

                       SendMessage(MessageTypes.POTHOLE_DETECTED,String.valueOf(PotholeID));

                        for(int i = 0 ; i < numberOfImg ; i++)
                        {
                            int sizeOfImage = input.readInt();
                            //input.readInt();
                            //if(sizeOfImage<0){ i--;continue;};

                            Log.d(TAG,"Size of array=" + sizeOfImage);
                            data = new byte[sizeOfImage];
                            input.readFully(data,0,data.length);

                            // send an ack to matlab.

                            Log.d(TAG,"Data===" + data.toString());
                            String filename = "pothole_"+PotholeID+"_"+count+".jpeg";

                            FileOutputStream fos = new FileOutputStream(RfcommActivity2.POTHOLE_FILE_PATH.getPath()+"/"+filename);
                            fos.write(data);
                            fos.flush();
                            fos.close();
                            //Log.d(TAG,"Client:" + threadCount + "=" + "File Created! = " + count);

                            count++;
                           // String full_file_path = RfcommActivity2.POTHOLE_FILE_PATH.getPath()+"/"+filename;
                          //  Bitmap bitmap = BitmapFactory.decodeFile(full_file_path);

                            //Log.d(TAG,"sending image....");

                            UploadToServer server = new UploadToServer();
                            server.send(data,filename,PotholeID); //this nameOfImage

                            //File file = new File(android.os.Environment.getExternalStorageDirectory(),"POTHOLE/"+filename);
                            //if(file.exists())
                            {
                                //data.toString().to
                               // Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                               // file.delete();
                            }

                            // remove the file from sd card..will be implemented later.

                            SendMessage(MessageTypes.IMAGE_RECEIVED,filename);

                            //return;
                        }
                    }
                }catch (Exception ex){
                    Log.d(TAG,ex.toString());
                    //return;
                }
            }
        }
    }
}

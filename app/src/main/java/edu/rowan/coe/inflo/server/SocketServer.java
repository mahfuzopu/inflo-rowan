package edu.rowan.coe.inflo.server;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;



import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by mdmhafi on 6/6/2016.
 */
public class SocketServer implements Runnable {
    public static final String TAG = "SocketServer";
    public static Boolean isServerRunning = false;
    public static int port = 8878;
    private Socket serverReadSocket ;

    Handler mHandler;
    public SocketServer(Handler handler)
    {
        mHandler = handler;
        SendMessage(MessageTypes.OUTPUT,"Server Constructor!");
        StartServerThread();
    }
    private void SendMessage(int type,String message)
    {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(type);
        Bundle bundle = new Bundle();
        bundle.putString(MessageTypes.DATA, message);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }
    public boolean StartServerThread()
    {
        try {
            if(isServerRunning)return  false;
            isServerRunning = true;
            Thread thread = new Thread(this);
            thread.start();
        }catch (Exception err) {
            return  false;
        }
        return  true;
    }
    public boolean StopServerThread()
    {
        if(isServerRunning) {
            isServerRunning = false;
            return  true;
        }
        return  false;
    }
    @Override
    public void run() {
        int threadCount  = 0;
        ServerSocket serverSocket;
        try {
             serverSocket =  new ServerSocket(this.port);
             SendMessage(MessageTypes.OUTPUT,"Waiting for client!");
        }catch (Exception ex){return;}

        while(isServerRunning)
        {
            try{
                serverReadSocket = serverSocket.accept();
                ServerReadWriteThread thread = new ServerReadWriteThread(serverReadSocket,++threadCount,mHandler);
                Log.d(TAG,"A new incoming connection!:" +  serverReadSocket.getInetAddress().getHostAddress());
                SendMessage(MessageTypes.OUTPUT,"A new client connected:!" + serverReadSocket.getInetAddress().getHostAddress());

            }catch (Exception err)
            {
            }
        }
    }


}

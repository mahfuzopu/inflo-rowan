package edu.rowan.coe.inflo.ui;

import android.graphics.Bitmap;

/**
 * Created by guest1 on 10/29/2015.
 */
public class ChatImage extends ChatItem {
    private Bitmap bitmap;
    public ChatImage(String message, String from, long time, Bitmap bitmap) {
        super(message, from, time);
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

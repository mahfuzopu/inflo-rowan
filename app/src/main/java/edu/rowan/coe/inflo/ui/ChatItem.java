package edu.rowan.coe.inflo.ui;

import android.graphics.Bitmap;

/**
 * Created by guest1 on 10/29/2015.
 */
public class ChatItem {
    private String message;
    private String from;
    private long time;

    public ChatItem(String message, String from, long time) {
        this.message = message;
        this.from = from;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public String getFrom() {
        return from;
    }

    public long getTime() {
        return time;
    }
}

package edu.rowan.coe.inflo.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import edu.rowan.coe.inflo.R;

/**
 * Created by guest1 on 10/29/2015.
 */
public class ChatRecyclerView extends RecyclerView.Adapter<ChatRecyclerView.ViewHolder> {
    private ChatItem[] mDataset;
    public final static int TYPE_MESSAGE = 2;
    public final static int TYPE_IMAGE = 3;

// Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mTextView;
        public int type;

        public ViewHolder(LinearLayout v, int type) {
            super(v);
            mTextView = v;
            this.type = type;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatRecyclerView(ArrayList<ChatItem> myDataset) {
        mDataset = myDataset.toArray(new ChatItem[myDataset.size()]);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch(viewType) {
            case TYPE_IMAGE:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_image, parent, false);
                break;
            case TYPE_MESSAGE:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message, parent, false);
                break;
        }
        ViewHolder vh = new ViewHolder((LinearLayout) v, viewType);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch(holder.getItemViewType()) {
            case TYPE_MESSAGE:
                ((TextView) holder.itemView.findViewById(R.id.message_txt)).setText(mDataset[position].getMessage());
                ((TextView) holder.itemView.findViewById(R.id.message_name)).setText(mDataset[position].getFrom());
                ((TextView) holder.itemView.findViewById(R.id.message_time)).setText(mDataset[position].getTime()+"");
                break;
            case TYPE_IMAGE:
                ((ImageView) holder.itemView.findViewById(R.id.message_image)).setImageBitmap(((ChatImage) mDataset[position]).getBitmap());
                ((TextView) holder.itemView.findViewById(R.id.message_name)).setText(mDataset[position].getFrom());
                ((TextView) holder.itemView.findViewById(R.id.message_time)).setText(mDataset[position].getTime()+"");
                break;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
    @Override
    public int getItemViewType(int pos) {
        if(mDataset[pos] instanceof ChatMessage)
            return TYPE_MESSAGE;
        else if(mDataset[pos] instanceof ChatImage)
            return TYPE_IMAGE;
        return TYPE_MESSAGE;
    }
}

package edu.rowan.coe.inflo.ui;

import android.util.Log;
import android.util.StringBuilderPrinter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;

import java.sql.Time;

/**
 * Created by mdmhafi on 8/11/2016.
 */
public class UploadPotholeToServer {
    private  static String URL = "http://pothole.widelinktech.com/pothole_upload.php";
    RequestParams params = new RequestParams();
    public void upload(int pid,String vid,Double longitude, Double latitude,int Timestamp, Double speed, Double accuracy, Double temperature,Double humidity)
    {
        params.put("pid",String.valueOf(pid));
        params.put("VId",vid);
        params.put("Longitude",String.valueOf(longitude));
        params.put("Latitude",String.valueOf(latitude));
        params.put("Timestamp", String.valueOf(Timestamp));
        params.put("Speed",String.valueOf(speed));
        params.put("Accuracy",String.valueOf(accuracy));
        params.put("Temperature",String.valueOf(temperature));
        params.put("Humidity",String.valueOf(humidity));
        makeHTTPCall();
    }
    public void makeHTTPCall() {
        // prgDialog.setMessage("Invoking Php");
        Log.d("SERVER","requesting http call..");
        AsyncHttpClient client = new AsyncHttpClient();
        // Don't forget to change the IP address to your LAN address. Port no as well.
        client.post(URL,
                params, new AsyncHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(String response) {
                        // Hide Progress Dialog
                        //  prgDialog.hide();
                        Log.d("SERVER",response);
                        //                   Toast.makeText(context.getApplicationContext(), response,
                        //                           Toast.LENGTH_LONG).show();
                        Log.e("SERVER",response);

                    }
                    // When the response returned by REST has Http
                    // response code other than '200' such as '404',
                    // '500' or '403' etc
                    @Override
                    public void onFailure(int statusCode, Throwable error,
                                          String content) {
                        // Hide Progress Dialog
                        //  prgDialog.hide();
                        Log.d("ERROR","Code=" + statusCode + ":" + error + "\n" + content);
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            //                     Toast.makeText(context.getApplicationContext(),
                            //                             "Requested resource not found",
                            //                             Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d("ERROR","Server erro code 500");
                            //                     Toast.makeText(context.getApplicationContext(),
                            //                             "Something went wrong at server end",
                            //                             Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            //                     Toast.makeText(
                            //                             context.getApplicationContext(),
                            //                             "Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "
                            //                                     + statusCode, Toast.LENGTH_LONG)
                            //                             .show();
                        }
                    }
                });
    }
}
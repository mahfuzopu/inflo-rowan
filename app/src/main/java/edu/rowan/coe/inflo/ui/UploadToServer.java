package edu.rowan.coe.inflo.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayOutputStream;
import java.io.File;

import edu.rowan.coe.inflo.ObuLocation;
import edu.rowan.coe.inflo.RfcommActivity2;

/**
 * Created by mdmhafi on 5/26/2016.
 */
public class UploadToServer {
    private static String TAG = "UploadToServer";
    Context context ;
    ProgressDialog prgDialog ;
    String encodedString;
    RequestParams params = new RequestParams();
    String imgPath, fileName;
    Bitmap bitmap;
    int PotholeId=0;
    private static int RESULT_LOAD_IMG = 1;
    //this url is for uploading images to server
    private static String URL_UPLOAD_POTHOLE = "http://pothole.widelinktech.com/testing/test.php";
    private static String URL_UPLOAD_POTHOLE_IMAGES = "http://pothole.widelinktech.com/image_upload.php";
    private static String URL_LOCAL_IMAGE_UPLOAD = "http://198.21.161.211:8080/rowan/image/test.php";
    private static String URL_UPLOAD_LIVE_DATA = "";
    public UploadToServer(Context con)
    {
        this.context = con;
        this.prgDialog = new ProgressDialog(context);
        this.prgDialog.setCancelable(true);
    }

    public UploadToServer()
    {
        //nothing
    }

    public void send(byte[] b,String filename,int potholeId)
    {

        this.fileName = filename;
        this.PotholeId = potholeId;
        Log.d("UploadToServer","in send method!");
        // if pothole id already exists then use this method.
        // and delete the file afterwards from the sdcard.
        String full_file_path = RfcommActivity2.POTHOLE_FILE_PATH.getPath()+"/"+filename;
        Bitmap bitmap = BitmapFactory.decodeFile(full_file_path);
        encodedString = BitMapToString(b,filename,bitmap);

        //this.bitmap = BitMapToString(bitmap);
        new AsyncTask<Void,Void,String>(){
            @Override
            protected String doInBackground(Void... params) {

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                params = new RequestParams();
                params.put("potholeID",String.valueOf(PotholeId));
                params.put("image",encodedString);
                params.put("filename",fileName);
                params.put("imgDetails","long/lat");
                params.put("imgType","png");

                triggerImageUpload();
                deleteImageFile(fileName);

            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
        }.execute(null,null,null);

        Log.d("UploadToServer","Encodded String:" + encodedString);
        Log.d("UploadToServer","FileName:" + fileName);
        Log.d("UploadToServer","PotholeId:" + potholeId);

    }

    public void send(Bitmap image,String image_name) {
        this.bitmap = image;
        this.fileName = image_name;
        params.put("filename",this.fileName);
        params.put("imgDetails","long/lat");
        params.put("imgType","jpeg");

        // get the current location and send it to server.
        // get the speed and send it ot server
        // get the timestamp and send it to server.

        // Double longi = ObuLocation.getInstance().getLatitude();
        // params.put("longitude",longi);

        // When Image is selected from Gallery
        if (fileName != null && image!=null) {
            //prgDialog.setMessage("Converting Image to Binary Data");
//            prgDialog.show();
            Log.e("UploadToServer","Converting Image to Binary Data");
            // Convert image to String using Base64
            encodeImagetoString();
            // When Image is not selected from Gallery
        } else {
            //Toast.makeText(
            //        context.getApplicationContext(),
            //        "You must select image from gallery before you try to upload",
            //        Toast.LENGTH_LONG).show();
        }
    }
    public void sendLiveData()
    {
    }
    public void encodeImagetoString() {
        new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {
            };

            @Override
            protected String doInBackground(Void... params) {
                BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
                options.inSampleSize = 3;
                //bitmap = BitmapFactory.decodeFile(imgPath,
                //        options);
                //ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                //bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                //byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                //encodedString = Base64.encodeToString(byte_arr, 0);
                //encodedString = BitMapToString(bitmap);

                return "";
            }

            @Override
            protected void onPostExecute(String msg) {
             //   prgDialog.setMessage("Calling Upload");
                // Put converted Image string into Async Http Post param
                params.put("image", encodedString);
                //params.
                // Trigger Image upload
                triggerImageUpload();
                //deleteImageFile(fileName);

            }
        }.execute(null, null, null);
    }
    public String BitMapToString(byte[] b2,String fileName, Bitmap bitmap1){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap1.compress(Bitmap.CompressFormat.JPEG,10, baos);
        byte [] b=baos.toByteArray();
        String temp=null;
        try{
            System.gc();
            temp=Base64.encodeToString(b, Base64.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
        }catch(OutOfMemoryError e){
            baos=new  ByteArrayOutputStream();
            String full_file_path = RfcommActivity2.POTHOLE_FILE_PATH.getPath()+"/"+fileName;
            Bitmap bitmap = BitmapFactory.decodeFile(full_file_path);
            bitmap.compress(Bitmap.CompressFormat.JPEG,5, baos);
            b = baos.toByteArray();
            temp=Base64.encodeToString(b, Base64.DEFAULT);
            Log.e("EWN", "Out of memory error catched");
        }
        return temp;
    }
    public void deleteImageFile(String fileName)
    {
        try{
            File file = new File(android.os.Environment.getExternalStorageDirectory(),"POTHOLE/"+fileName);
            if(file.exists())
            {
                file.delete();
            }
        }catch (Exception ex)
        {
            Log.e(TAG,ex.toString());
        }
    }

    public void triggerImageUpload() {
        makeHTTPCall();
    }

    // http://192.168.2.4:9000/imgupload/upload_image.php
    // http://192.168.2.4:9999/ImageUploadWebApp/uploadimg.jsp
    // Make Http call to upload Image to Php server
    public void makeHTTPCall() {
       // prgDialog.setMessage("Invoking Php");
        Log.d("SERVER","requesting http call..");
        AsyncHttpClient client = new AsyncHttpClient();
        // Don't forget to change the IP address to your LAN address. Port no as well.
        client.post(URL_UPLOAD_POTHOLE_IMAGES,
                params, new AsyncHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(String response) {
                        // Hide Progress Dialog
                      //  prgDialog.hide();
                        Log.d("SERVER",response);
     //                   Toast.makeText(context.getApplicationContext(), response,
     //                           Toast.LENGTH_LONG).show();
                        Log.e("SERVER",response);

                    }
                    // When the response returned by REST has Http
                    // response code other than '200' such as '404',
                    // '500' or '403' etc
                    @Override
                    public void onFailure(int statusCode, Throwable error,
                                          String content) {
                        // Hide Progress Dialog
                      //  prgDialog.hide();
                        Log.d("ERROR","Code=" + statusCode + ":" + error + "\n" + content);
                        // When Http response code is '404'
                        if (statusCode == 404) {
       //                     Toast.makeText(context.getApplicationContext(),
       //                             "Requested resource not found",
       //                             Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d("ERROR","Server erro code 500");
       //                     Toast.makeText(context.getApplicationContext(),
       //                             "Something went wrong at server end",
       //                             Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
       //                     Toast.makeText(
       //                             context.getApplicationContext(),
       //                             "Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "
       //                                     + statusCode, Toast.LENGTH_LONG)
       //                             .show();
                        }
                    }
                });
    }
}

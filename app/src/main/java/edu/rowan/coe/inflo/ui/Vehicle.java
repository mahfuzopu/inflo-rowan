package edu.rowan.coe.inflo.ui;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by mdmhafi on 6/20/2016.
 */
public class Vehicle implements Serializable{
    private String Id;
    private String time;
    private double longitude;
    private double latitude;
    private double speed;
    private int accuracy;
    private double pavementTemp;
    private int potholeId;

    public void resetDefaultSetting()
    {
        setId("NJDOT001");
        setLatitude(-82.8413570);
        setLongitude(34.6818290);
        setSpeed(0);
        setAccuracy(0);
        setPavementTemp(0);
        setPotholeId(0);
    }
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public double getPavementTemp() {
        return pavementTemp;
    }

    public void setPavementTemp(double pavementTemp) {
        this.pavementTemp = pavementTemp;
    }

    public int getPotholeId() {
        return potholeId;
    }

    public void setPotholeId(int potholeId) {
        this.potholeId = potholeId;
    }
}
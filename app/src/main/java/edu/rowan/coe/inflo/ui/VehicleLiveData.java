package edu.rowan.coe.inflo.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.rowan.coe.inflo.ObuLocation;
import edu.rowan.coe.inflo.server.MessageTypes;

/**
 * Created by mdmhafi on 6/20/2016.
 */
public class VehicleLiveData implements Runnable {
    private static final String TAG = "VehicleLiveData";
    public static boolean isVehicleLiveDataRunning = false;
    private int frequency = 1000;
    private static VehicleLiveData instance = null ;
    public Vehicle vehicle;
    public VehicleLiveData()
    {
        vehicle = new Vehicle();
        vehicle.resetDefaultSetting();
        startLiveData();
    }
    public static VehicleLiveData getInstance(){
        if(instance==null)
        {
            instance = new VehicleLiveData();
        }
        return instance;
    }
    private void startLiveData()
    {
        isVehicleLiveDataRunning = true;
        Thread thread = new Thread(this);
        thread.start();
    }
    @Override
    public void run() {
        while(isVehicleLiveDataRunning) {
            try {
                //collect vehicle data and send the notification to upper layer
                //to send the data to server.
                ObuLocation obuLocation = ObuLocation.getInstance();
                vehicle.setLatitude(obuLocation.getLatitude());
                vehicle.setLongitude(obuLocation.getLongitude());
                vehicle.setSpeed(obuLocation.getSpeed());
                Date currentDate = new Date(System.currentTimeMillis());
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = sdf.format(c.getTime());

                vehicle.setTime(strDate);

                SendData();

                Thread.sleep(frequency);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void stopLiveThread()
    {
        isVehicleLiveDataRunning  = false;
    }
    private void SendData()
    {
        RequestParams params = new RequestParams();
        params.put("VID",vehicle.getId());
        params.put("TIMESTAMP",vehicle.getTime());
        params.put("LATITUDE",vehicle.getLatitude()+"");
        params.put("LONGITUDE",vehicle.getLongitude()+"");
        params.put("SPEED",vehicle.getSpeed() );
        // params.put("ACCURACY",vehicle.getAccuracy());
        // params.put("PAVEMENT_TEMP",vehicle.getPavementTemp());
        // params.put("POTHOLE_ID",vehicle.getPotholeId());

        //String URL = "http://pothole.widelinktech.com/testing/test_live_data.php";

        String URL = "http://pothole.widelinktech.com/testing/livetracking/api/update_current_location.php";
        //made aysnc call to send data.
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(URL,params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(String content) {
                //Log.d(TAG,"Packet sent with:(" + vehicle.getId()+","+ vehicle.getLongitude()+","+ vehicle.getLatitude()+")");
                //Log.e(TAG,content);
            }
        });
    }
}
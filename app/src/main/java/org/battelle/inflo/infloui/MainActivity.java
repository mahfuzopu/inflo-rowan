/**
 * @file         infloui/MainActivity.java
 * @author       Joshua Branch
 * 
 * @copyright Copyright (c) 2013 Battelle Memorial Institute. All rights reserved.
 * 
 * @par
 * Unauthorized use or duplication may violate state, federal and/or
 * international laws including the Copyright Laws of the United States
 * and of other international jurisdictions.
 * 
 * @par
 * @verbatim
 * Battelle Memorial Institute
 * 505 King Avenue
 * Columbus, Ohio  43201
 * @endverbatim
 * 
 * @brief
 * TBD
 * 
 * @details
 * TBD
 */

package org.battelle.inflo.infloui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import org.battelle.inflo.infloui.ui.TabsAdapter;

import edu.rowan.coe.inflo.R;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity {
	String TAG = "inflo:MA";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);

		startService(new Intent(this, ApplicationMonitorService.class));

//		TabHost tabHost = getTabHost();

		/*TabSpec alertTab = tabHost.newTabSpec(getResources().getString(
				R.string.activity_dashboard));
		Intent alertsIntent = new Intent(this, DashboardActivity.class);
		alertTab.setIndicator(getResources().getString(
				R.string.activity_dashboard));
		alertTab.setContent(alertsIntent);

		TabSpec diagnosticsTab = tabHost.newTabSpec(getResources().getString(
				R.string.activity_diagnostics));
		Intent diagnosticsIntent = new Intent(this, DiagnosticsActivity.class);
		diagnosticsTab.setIndicator(getResources().getString(
				R.string.activity_diagnostics));
		diagnosticsTab.setContent(diagnosticsIntent);

		tabHost.addTab(alertTab);
		tabHost.addTab(diagnosticsTab);*/

		TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
		TabLayout.Tab alertTab = tabLayout.newTab().setText(R.string.activity_dashboard);
		final Intent alertsIntent = new Intent(this, DashboardActivity.class);

		TabLayout.Tab diagnosticsTab = tabLayout.newTab().setText(R.string.activity_diagnostics);
		final Intent diagnosticsIntent = new Intent(this, DiagnosticsActivity.class);

		tabLayout.addTab(alertTab);
		tabLayout.addTab(diagnosticsTab);
		/*tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				Log.d(TAG, "Tab "+tab.getText()+" seleted");
				if(tab.getText().equals(getString(R.string.activity_dashboard))) {
					startActivity(alertsIntent);
				} else {
					startActivity(diagnosticsIntent);
				}
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
				Log.d(TAG, "Tab "+tab.getText()+" reseleted");
			}
		});*/
//		tabLayout.setTabMode(TabLayout.MODE_FIXED);
		((ViewPager) findViewById(R.id.viewpager)).setAdapter(new TabsAdapter(this, ((ViewPager) findViewById(R.id.viewpager))));
		tabLayout.setupWithViewPager((ViewPager) findViewById(R.id.viewpager));
		((ViewPager) findViewById(R.id.viewpager)).addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
		tabLayout.setTabsFromPagerAdapter(((ViewPager) findViewById(R.id.viewpager)).getAdapter());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	public void onSettingsMenuClick(MenuItem item) {
		startActivity(new Intent(this, SettingsActivity.class));
	}

	public void onCloseMenuClick(MenuItem item) {
		finish();
		stopService(new Intent(this, ApplicationMonitorService.class));
	}

	public void onConsoleMenuClick(MenuItem item) {
		startActivity(new Intent(this, ConsoleActivity.class));
	}

	public void onGenerateSpdHarmAlert(View v) {
		Log.d(TAG, "Generate SPD Harm Alert");
	}
	public void onGenerateQWarnAlert(View v) {
		Log.d(TAG, "Generate QWarn Alert");
	}
}

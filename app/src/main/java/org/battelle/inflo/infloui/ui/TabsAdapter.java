package org.battelle.inflo.infloui.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import org.battelle.inflo.infloui.DashboardActivity;
import org.battelle.inflo.infloui.DiagnosticsActivity;

import java.util.ArrayList;

import edu.rowan.coe.inflo.R;

/**
 * Created by Nick on 9/9/2015.
 */
public class TabsAdapter extends FragmentPagerAdapter
        implements ViewPager.OnPageChangeListener {
    private final AppCompatActivity mContext;
    private final ActionBar mActionBar;
    private final ViewPager mViewPager;
    private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

    static final class TabInfo {
        private final Class<?> clss;
        private final Bundle args;

        TabInfo(Class<?> _class, Bundle _args) {
            clss = _class;
            args = _args;
        }
    }

    public TabsAdapter(AppCompatActivity activity, ViewPager pager) {
        super(activity.getSupportFragmentManager());
        mContext = activity;
        mActionBar = activity.getSupportActionBar();
        mViewPager = pager;
        mViewPager.setAdapter(this);
        mViewPager.setOnPageChangeListener(this);

    }

    public void addTab(TabLayout.Tab tab, Class<?> clss, Bundle args) {
//        TabInfo info = new TabInfo(clss, args);
//        tab.setTag(info);
//        tab.setTabListener(this);
//        mTabs.add(info);
//        mActionBar.addTab(tab);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
//        return mTabs.size();
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int pos) {
        switch(pos) {
            case 0:
                return mContext.getString(R.string.activity_dashboard);
            case 1:
                return mContext.getString(R.string.activity_diagnostics);
        }
        return "Null";
    }

    @Override
    public Fragment getItem(int position) {
//        TabInfo info = mTabs.get(position);
//        return Fragment.instantiate(mContext, info.clss.getName(), info.args);
        Fragment tab;
        if(position == 0)
            tab = new DashboardActivity();
        else
            tab = new DiagnosticsActivity();
//        mContext.getSupportFragmentManager().putFragment(null, "Helllo", tab);
        return tab;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
//        mActionBar.setSelectedNavigationItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
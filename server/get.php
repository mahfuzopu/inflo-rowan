<?php

include('connect.php');
$id = intval($_GET['id']);
$query = mysql_query("SELECT * FROM `inflo_v1` WHERE `id` = $id");
if(strlen($_GET['id']) == 0)
    $query = mysql_query("SELECT * FROM `inflo_v1` ORDER BY `id` DESC LIMIT 1");
    
while($row = mysql_fetch_assoc($query)) {
    $payload = $row['payload'];
//    echo $payload;
}
?>
<html>
    <head>
        <title>INFLO App</title>
        <link href='styles.css' rel="stylesheet"/>
        <link rel="shortcut icon" type="image/png" href="icon.png"/>
    </head>
    <body>
        <div id='menubar'>
            <ul>
                <li id='icon'><img src='icon.png'/></li>
                <li id='title'>INFLO SensorHub</li>
            </ul>
                <span id='copyright'>Rowan University 2015</span>
        </div>
        
        <h3 id='date'></h3>
<!--        <h3 id='sensorlevel'>From SensorDevice</h3>-->
        <h3 id='priority'></h3>
        <span class='latlong' id='speed'></span>
        <span class='latlong' id='speedUnits'>mph</span>        
        
        <img id='kinect_color'/>
        
        <div id='notes'></div>        
        
        <button id='goPrevious'>Previous</button>
        <button id='goNext'>Next</button>
        
        <div id='coordinates'>
            <span class='latlong' id='latitude'></span>
            <span class='latlong' id='latlongdelimiter'>, </span>
            <span class='latlong' id='longitude'></span>
        </div>
        
        <iframe id='gmaps' frameborder="0" style="border:0"
src="" allowfullscreen></iframe>
        <script>
            var GPS_LATITUDE = 122;
            var GPS_LONGITUDE = 121;
            var GPS_SPEED = 123;
            var KINECT_COLOR = 102;
            var PLAINTEXT = 999;
            var PRIORITY_LOW = 11;
            var SENSOR_HUB = 2;
            
            var id = <?php echo $id; ?>;
            var dataarray = JSON.parse('<?php echo $payload; ?>');
            var myData = dataarray[0];
            
            //I have my JSON object, now I populate the page
            if(myData.time !== undefined) {
                document.getElementById('date').innerHTML = formatDate(myData.time);
            }
            
            if(myData.priority == PRIORITY_LOW) {
                document.getElementById('priority').innerHTML = "Low Priority";   
            }
            
            if(myData[KINECT_COLOR] !== undefined) {
                document.getElementById('kinect_color').src = 'data:image/bitmap;base64,'+myData[KINECT_COLOR];   
            }
            
            if(myData[GPS_LATITUDE] !== undefined) {
                document.getElementById('gmaps').src = getGmapsUrl(myData[GPS_LATITUDE], myData[GPS_LONGITUDE]);  
                document.getElementById('latitude').innerHTML = myData[GPS_LATITUDE];
                document.getElementById('longitude').innerHTML = myData[GPS_LONGITUDE];
            }
            if(myData[GPS_SPEED] !== undefined) {
                document.getElementById('speed').innerHTML = myData[GPS_SPEED];   
            }   
            
            if(myData[PLAINTEXT] !== undefined) {
                document.getElementById('notes').innerHTML = myData[PLAINTEXT];       
            }
            
            
            
            /*if(id == 0)
                document.getElementById('goPrevious').style.opacity = "0";*/
            
            document.getElementById('goPrevious').onclick = function() {
                window.location.href = "get.php?id="+(id-1);   
            }
            
            document.getElementById('goNext').onclick = function() {
                window.location.href = "get.php?id="+(id+1);   
            }
            
            function getGmapsUrl(lat, long) {
                /*return "https://www.google.com/maps/embed/v1/view?zoom=16&center="+lat+","+long;  */
                return "http://www.openstreetmap.org/export/embed.html?bbox="+(lat-0)+","+(long-0)+","+(lat+0.01)+","+(long+0.005);
            }
            
            function formatDate(time) {
                var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
                ];

                var date = new Date(time);
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();     
                
                var hour = (date.getHours() < 10)?"0"+date.getHours():date.getHours();
                var minute = (date.getMinutes() < 10)?"0"+date.getMinutes():date.getMinutes();
                
                return monthNames[monthIndex]+" "+day+", "+year+"&emsp;"+hour+":"+minute;
            }
        </script>
    </body>
</html>